﻿namespace Bunny.AutoMapper
{
    public class Enums
    {
        public enum MapperResolveType
        {
            PerRequest = 1,
            PreResolve = 2,
            Singleton = 3
        }
    }
}