﻿using System;
using Bunny.Business.Interface.InitialData;
using Bunny.Core.EnumUtils;
using Bunny.Core.StringUtils;
using Bunny.Data.EF.Entities;
using Bunny.Data.EF.IRepository.Role;
using Bunny.Data.EF.IRepository.User;
using Bunny.DependencyInjection.Attributes;
using Bunny.Middle;
using Bunny.Middle.AppConfigs;
using Microsoft.EntityFrameworkCore.Internal;

namespace Bunny.Business.Implement.InitialData
{
    [PerRequestDependency(ServiceType = typeof(IInitialBusiness))]
    public class InitialBusiness : IInitialBusiness
    {
        public readonly IUserRepository _userRepository;
        public readonly IRoleRepository _roleRepository;

        public InitialBusiness(IUserRepository userRepository, IRoleRepository roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }
        public void DummyData()
        {
            DummyRole();
            DummyUser();
        }

        #region private class

        private void DummyRole()
        {
            var listRole = EnumHelper.GetListEnum<Enums.Role>();
            foreach (var role in listRole)
            {
                var m = role;
                var newGuid = Guid.NewGuid();
                if (!_roleRepository.Get(p => p.Type == role).Any())
                {
                    _roleRepository.Add(new RoleEntity
                    {
                        GlobalId = newGuid.ToString(),
                        Type = role,
                        Name = role.GetDisplayName(),
                        Description = role.GetDescription(),
                    });
                    _roleRepository.SaveChanges();
                }
            }
        }
        private void DummyUser()
        {
            if (!_userRepository.Get().Any())
            {
                //Get admin role
                var role = _roleRepository.GetSingle(p => p.Type == Enums.Role.Admin);
                //Add user
                var key = AppConfigs.DataHashConfig.Key;
                var newGuid = Guid.NewGuid();
                _userRepository.Add(new UserEntity
                {
                    GlobalId = newGuid.ToString(),
                    Subject = newGuid.ToString(),
                    UserName = BusinessConstant.DummyUser.DefaultUserName,
                    Password = BusinessConstant.DummyUser.DefaultPassword.Encrypt(key),
                    LastName = "Vu",
                    FirstName = "Le",
                    PhoneNumber = "0857488989",
                    RoleId = role?.Id ?? 0,
                });
                _userRepository.SaveChanges();
            }
            
        }

        #endregion private class
    }
}