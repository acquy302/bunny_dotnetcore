﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Business.Interface.User;
using Bunny.Data.EF.IRepository.User;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.User;

namespace Bunny.Business.Implement.User
{
    [PerRequestDependency(ServiceType = typeof(IUserBusiness))]
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;

        public UserBusiness(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<DataTableResponseDataModel<UserDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default)
        {
            var listData = _userRepository.Get().QueryTo<UserDataTableModel>();
            var result = listData.GetDataTableResponse(model);
            return Task.FromResult(result);
        }

        public Task<List<UserViewModel>> GetAllUser()
        {
            var result = Mapper.Map<List<UserViewModel>>(_userRepository.Get().ToList());
            return Task.FromResult(result);
        }

        public Task<UserEditModel> GetEdit(int id, CancellationToken cancellationToken = default)
        {
            var entity = _userRepository.GetSingle(p => p.Id == id);
            return Task.FromResult(Mapper.Map<UserEditModel>(entity));
        }
    }
}