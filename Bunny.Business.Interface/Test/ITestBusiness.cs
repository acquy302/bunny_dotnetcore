﻿using System.Collections.Generic;
using System.Threading;
using Bunny.Data.EF.Entities;
using System.Threading.Tasks;
using Bunny.Model.User;

namespace Bunny.Business.Interface.Test
{
    public interface ITestBusiness
    {
        Task<UserEntity> TestAddEntity(CancellationToken cancellationToken = default);
        Task<List<UserViewModel>> GetAllUserEntity(CancellationToken cancellationToken = default);
    }
}