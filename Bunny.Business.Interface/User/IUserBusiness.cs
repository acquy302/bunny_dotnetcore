﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.Model.User;

namespace Bunny.Business.Interface.User
{
    public interface IUserBusiness
    {
        Task<DataTableResponseDataModel<UserDataTableModel>> GetDataTableAsync(DataTableParamModel model, CancellationToken cancellationToken = default);
        Task<List<UserViewModel>> GetAllUser();
        Task<UserEditModel> GetEdit(int id, CancellationToken cancellationToken = default);
    }
}