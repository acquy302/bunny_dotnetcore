﻿using System;
using System.Globalization;

namespace Bunny.Core.DateTimeUtils
{
    public static class DateTimeHelper
    {
        public static readonly DateTime EpochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime Parse(string value, string format)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException(nameof(value));

            return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
        }

        public static bool TryParse(string value, string format, out DateTime dateTime)
        {
            return DateTime.TryParseExact(value, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
        }

        public static DateTime? ReplaceNullOrDefault(DateTime? value, DateTime? replace)
        {
            value = value ?? replace;
            return value;
        }

        public static DateTime ReplaceNullOrDefault(DateTime value, DateTime replace)
        {
            value = value == default ? replace : value;
            return value;
        }

        public static DateTimeOffset? ReplaceNullOrDefault(DateTimeOffset? value, DateTimeOffset? replace)
        {
            value = value ?? replace;
            return value;
        }

        public static DateTimeOffset ReplaceNullOrDefault(DateTimeOffset value, DateTimeOffset replace)
        {
            value = value == default ? replace : value;
            return value;
        }

        /// <summary>
        ///     Return a Date Time from Epoch Time plus value as total seconds 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTimeOffset GetDateTimeFromEpoch(double value)
        {
            return EpochTime.AddSeconds(value);
        }
    }
}