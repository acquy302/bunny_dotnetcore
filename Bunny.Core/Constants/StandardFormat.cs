﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Bunny.Core.Constants
{
    public class StandardFormat
    {
        /// <summary>
        ///     Isolate Datetime StandardFormat 
        /// </summary>
        public const string DateTimeOffSetFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK";

        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.None,
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            DateFormatString = DateTimeOffSetFormat,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };
    }
}