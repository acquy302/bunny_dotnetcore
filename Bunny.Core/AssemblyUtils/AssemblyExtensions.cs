﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Bunny.Core.AssemblyUtils
{
    public static class AssemblyExtensions
    {
        public static IEnumerable<T> GetImplementationsOf<T>(this Assembly assembly)
        {
            var types = assembly.GetTypes()
                .Where(t => t.GetTypeInfo().IsClass && !t.GetTypeInfo().IsAbstract && typeof(T).IsAssignableFrom(t))
                .ToList();

            return types.Select(type => (T)Activator.CreateInstance(type)).ToList();
        }

        public static IEnumerable<T> GetImplementationsOf<T>(this IEnumerable<Assembly> assemblies)
        {
            var result = new List<T>();

            foreach (var assembly in assemblies)
            {
                result.AddRange(assembly.GetImplementationsOf<T>());
            }

            return result;
        }

        public static string GetDirectoryPath(this Assembly assembly)
        {
            UriBuilder uri = new UriBuilder(assembly.CodeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }
    }
}