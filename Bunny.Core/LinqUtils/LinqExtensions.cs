﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bunny.Core.LinqUtils
{
    public static class LinqExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var seenKeys = new HashSet<TKey>();
            foreach (var element in source)
                if (seenKeys.Add(keySelector(element)))
                    yield return element;
        }

        public static IEnumerable<TSource> RemoveWhere<TSource>
            (this IEnumerable<TSource> source, Predicate<TSource> predicate)
        {
            return source.Where(x => !predicate(x));
        }
    }
}