﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.RefreshToken
{
    public interface IRefreshTokenRepository : IEntityRepository<RefreshTokenEntity>
    {
        
    }
}