﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.User
{
    public interface IUserRepository : IEntityRepository<UserEntity>
    {
        
    }
}