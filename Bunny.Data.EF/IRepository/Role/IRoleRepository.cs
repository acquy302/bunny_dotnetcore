﻿using Bunny.Data.EF.Entities;

namespace Bunny.Data.EF.IRepository.Role
{
    public interface IRoleRepository : IEntityRepository<RoleEntity>
    {
        
    }
}