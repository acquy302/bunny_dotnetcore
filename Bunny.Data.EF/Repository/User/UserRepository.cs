﻿using Bunny.Data.EF.Entities;
using Bunny.Data.EF.Implement;
using Bunny.Data.EF.Interface;
using Bunny.Data.EF.IRepository.User;
using Bunny.DependencyInjection.Attributes;

namespace Bunny.Data.EF.Repository.User
{
    [PerRequestDependency(ServiceType = typeof(IUserRepository))]
    public class UserRepository : EntityRepository<UserEntity>, IUserRepository
    {
        public UserRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}