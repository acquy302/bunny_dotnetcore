﻿using System.Collections.Generic;
using Bunny.Data.EF.Entities.Base;
using Bunny.Middle;

namespace Bunny.Data.EF.Entities
{
    public class RoleEntity : Entity
    {
        public Enums.Role Type { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserEntity> User { get; set; }
    }
}