﻿using Bunny.Data.EF.Entities;
using Bunny.EF.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bunny.Data.EF.MapConfiguration.Role
{
    public class RoleConfiguration : DbEntityConfiguration<RoleEntity>
    {
        public override void Configure(EntityTypeBuilder<RoleEntity> entity)
        {
            entity.HasKey(c => c.Id);

            entity.HasMany(c => c.User).WithOne(c => c.Role).HasForeignKey(c => c.RoleId);
        }
    }
}