﻿using System;
using Microsoft.AspNetCore.Builder;

namespace Bunny.Data.EF.Interface
{
    public interface IDatabaseFactory
    {
        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        IServiceProvider MigrateDatabase(IServiceProvider services);

        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        IApplicationBuilder MigrateDatabase(IApplicationBuilder app);
    }
}