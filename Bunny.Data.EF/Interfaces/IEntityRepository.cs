﻿using Bunny.Data.EF.Entities.Base;
using Bunny.EF.Repositories;

namespace Bunny.Data.EF
{
    public interface IEntityRepository<TEntity> : IEntityRepository<TEntity, int> where TEntity : Entity, new()
    {
        
    }
}