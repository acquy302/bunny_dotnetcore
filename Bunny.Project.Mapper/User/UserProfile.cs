﻿using AutoMapper;
using Bunny.AutoMapper;
using Bunny.Data.EF.Entities;
using Bunny.Middle.Model.User;
using Bunny.Model.MasterPage;
using Bunny.Model.User;

namespace Bunny.Project.Mapper.User
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserViewModel, UserEntity>().IgnoreAllNonExisting();

            CreateMap<UserEntity, UserViewModel>().IgnoreAllNonExisting();

            CreateMap<UserEntity, LoggedInUserModel>().IgnoreAllNonExisting();

            CreateMap<UserEntity, UserHeaderViewModel>().IgnoreAllNonExisting();

            CreateMap<UserEntity, UserDataTableModel>().IgnoreAllNonExisting();

            CreateMap<UserLoginModel, RequestTokenModel>().IgnoreAllNonExisting()
                .ForMember(d => d.UserName, o => o.MapFrom(s => s.UserName))
                .ForMember(d => d.Password, o => o.MapFrom(s => s.Password));
            //Edit User
            CreateMap<UserEditModel, UserEntity>().IgnoreAllNonExisting();

            CreateMap<UserEntity, UserEditModel>().IgnoreAllNonExisting();
        }
    }
}