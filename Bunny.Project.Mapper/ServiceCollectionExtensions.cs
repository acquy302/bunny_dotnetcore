﻿using Bunny.AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.Project.Mapper
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAutoMapperBunny(this IServiceCollection services)
        {
            services.AddAutoMapper(isAssertConfigurationIsValid: true, isCompileMappings: true, profileAssemblyMarkerTypes: typeof(IAutoMapperProfile));

            return services;
        }
    }
}