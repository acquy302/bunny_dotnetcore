﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bunny.Business.Interface.Test;
using Bunny.Data.EF.Entities;
using Bunny.DependencyInjection.Attributes;
using Bunny.Model.User;
using Bunny.Service.Interface.Test;

namespace Bunny.Service.Implement.Test
{
    [PerRequestDependency(ServiceType = typeof(ITestService))]
    public class TestService : ITestService
    {
        private readonly ITestBusiness _testBusiness;

        public TestService(ITestBusiness testBusiness)
        {
            _testBusiness = testBusiness;
        }
        public Task<UserEntity> TestAddEntity(CancellationToken cancellationToken = default)
        {
            return _testBusiness.TestAddEntity(cancellationToken);
        }

        public Task<List<UserViewModel>> GetAllUserEntity(CancellationToken cancellationToken = default)
        {
            return _testBusiness.GetAllUserEntity(cancellationToken);
        }
    }
}