﻿using Bunny.Business.Interface.InitialData;
using Bunny.DependencyInjection.Attributes;
using Bunny.Service.Interface.InitialData;

namespace Bunny.Service.Implement.InitialData
{
    [PerRequestDependency(ServiceType = typeof(IInitialService))]
    public class InitialService : IInitialService
    {
        private readonly IInitialBusiness _initialBusiness;

        public InitialService(IInitialBusiness initialBusiness)
        {
            _initialBusiness = initialBusiness;
        }
        public void DummyData()
        {
            _initialBusiness.DummyData();
        }
    }
}