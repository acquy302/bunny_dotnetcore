﻿using System;

namespace Bunny.Model.MasterPage
{
    public class AccessTokenModel
    {
        public string AccessToken { get; set; }

        /// <summary>
        ///     Expire on UTC 
        /// </summary>
        public DateTimeOffset? ExpireOn { get; set; }

        /// <summary>
        ///     Lifetime of token in seconds 
        /// </summary>
        public double? ExpireIn { get; set; }

        public string RefreshToken { get; set; }

        public string TokenType { get; set; }
    }
}