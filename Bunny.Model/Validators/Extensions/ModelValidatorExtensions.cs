﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.Model.Validators.Extensions
{
    public static class ModelValidatorExtensions
    {
        /// <summary>
        ///     [Validator] Model Validator, Must after "AddMvc" 
        /// </summary>
        /// <param name="mvcBuilder"></param>
        /// <returns></returns>
        public static IMvcBuilder AddModelValidator(this IMvcBuilder mvcBuilder)
        {
            // Enable Microsoft.jQuery.Unobtrusive.Validation for Front-end
            mvcBuilder.AddViewOptions(options =>
            {
                options.HtmlHelperOptions.ClientValidationEnabled = true;
            });

            // Register Fluent Validation Rules
            mvcBuilder.AddFluentValidation(fvc =>
            {
                fvc.RegisterValidatorsFromAssemblyContaining<IModelValidator>();
            });

            return mvcBuilder;
        }
    }
}