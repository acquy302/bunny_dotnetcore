﻿using Bunny.Model.User;
using FluentValidation;

namespace Bunny.Model.Validators.UserValidator
{
    public class UserModelValidator
    {
        public class UserLoginViewModelValidator : AbstractValidator<UserLoginModel>
        {
            public UserLoginViewModelValidator()
            {
                RuleFor(x => x.UserName).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }
    }
}