﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Bunny.Data.EF.Extensions;
using Bunny.DependencyInjection;
using Bunny.Middle;
using Bunny.Middle.Configs;
using Bunny.Service.Interface.InitialData;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using static Bunny.Middle.Configs.SystemConfigServiceExtensions;

namespace Bunny
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildConfiguration();

            var host = BuildWebHost(args);

            OnApplicationStart(host);
            host.Run();
            //CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost
                .CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Warning))
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .UseIISIntegration()
                .Build();
        private static void BuildConfiguration()
        {
            // Build System Config at first time for config Root, in Startup will build again with reload update features
            var builder = new ConfigurationBuilder().AddJsonFile(Middle.Constants.Configuration.AppSettingsJsonFileName, true, false);

            IConfiguration configuration = builder.Build();

            SystemConfigurationHelper.BuildSystemConfig(configuration);
        }
        private static void OnApplicationStart(IWebHost host)
        {

            SystemConfig.SystemVersion = SystemUtils.SystemTimeNow;
            var serviceScopeFactory = (IServiceScopeFactory)host.Services.GetService(typeof(IServiceScopeFactory));

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var services = scope.ServiceProvider;
                services.MigrateDatabase();

                var initService = services.GetService<IInitialService>();
                initService.DummyData();
            }
        }
        
    }
}
