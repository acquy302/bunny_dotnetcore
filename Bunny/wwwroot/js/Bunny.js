﻿'use strict';

function inputNumber(sender) {
    sender.value = sender.value.replace(/[^\0-9]/ig, "");
}

function getQueryString() {
    var key = false, res = {}, itm = null;
    // get the query string without the ?
    var qs = location.search.substring(1);
    // check for the key as an argument
    if (arguments.length > 0 && arguments[0].length > 1)
        key = arguments[0];
    // make a regex pattern to grab key/value
    var pattern = /([^&=]+)=([^&]*)/g;
    // loop the items in the query string, either
    // find a match to the argument, or build an object
    // with key/value pairs
    while (itm = pattern.exec(qs)) {
        if (key !== false && decodeURIComponent(itm[1]) === key)
            return decodeURIComponent(itm[2]);
        else if (key === false)
            res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
    }

    return key === false ? res : null;
}

window.Bunny = {
    searchAjaxConfig: {
        skip: 0,
        take: 10
    },
    removeClassSuccess: function () {
        $(".content-modal-body").find(".has-success").removeClass("has-success");

        var $validation = $('[data-valid="validation-modal"]');

        $.each($validation, function (i, ele) {
            $(ele).parent().find(".error-message").remove();
        });

    },
    addInputJsonToForm: function (idForm, listFieldName, inputList, inputAttrib) {
        var htmlcontent = "";
        //var startIdx = 0;
        $(inputList).each(function (idx, item) {
            $(inputAttrib).each(function (id, attribute) {
                if ($("input[name='" + listFieldName + "[" + idx + "]." + attribute + "']").length > 0) {
                    $("input[name='" + listFieldName + "[" + idx + "]." + attribute + "']").remove();
                }
                var input = "<input name='" + listFieldName + "[" + idx + "]." + attribute + "' type='hidden' value=\"" + item[attribute] + "\"/>";
                htmlcontent += input;
            });
            //startIdx++;
        });

        $("#" + idForm).append(htmlcontent);
    },

    initToolTip: function () {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
    },

    initConfirmDialog: function () {
        $('[data-plugin="confirm"]').click(function () {
            var $this = $(this);

            swal({
                title: $this.data("confirm-title"),
                text: $this.data("confirm-message"),
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-pink",
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var action = $this.data("confirm-yes-callback");
                        var actionOk = $this.data("enable-loading");

                        eval(action);
                        $(".loader-overlay").remove();
                        swal({
                            title: $this.data("confirm-yes-title") || "Deleted!",
                            text: $this.data("confirm-yes-message") || "Delete Successful",
                            type: "success",
                            timer: 2000,

                        }, function () {
                            swal.close();
                            if (actionOk) {
                                window.Bunny.initLoading();
                            }
                        });
                    } else {
                        swal({
                            title: $this.data("confirm-no-title") || "Canceled",
                            text: $this.data("confirm-no-message"),
                            type: "error",
                            timer: 2000
                        });
                    }
                });
        });
    },

    initCountryDropdown: function () {
        var $countryList = $('select[data-plugin="country"]');
        $.each($countryList, function (i, ele) {
            var listCountry;
            var defaultValue = $(ele).attr("data-default");
            var defaultText = $(ele).attr("data-default-text");

            if (defaultValue !== "" && defaultValue != undefined) {
                $(ele).append('<option value="' + defaultValue + '">' + defaultText + '</option>');
            } else {
                $(ele).append('<option value="SG">SINGAPORE</option>');
            }

            $(ele).select2({
                tags: false,
                ajax: {
                    url: "/api/search/country",
                    data: function (params) {
                        var query = {
                            skip: window.Bunny.searchAjaxConfig.skip,
                            take: window.Bunny.searchAjaxConfig.take,
                            terms: params.term
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        listCountry = data == undefined ? [] : data.items;
                        return {
                            results: $.map(listCountry,
                                function (item) {
                                    return {
                                        text: item.text,
                                        id: item.value
                                    }
                                })
                        };
                    }
                }
            });

            $(ele).on("select2:select",
                function (e) {
                    if (e.target.value === $(this).find(":selected").text()) {
                        $(this).val($(this).find(":selected").text());
                    } else {
                        var index = 0;
                        for (var i = 0; i < listCountry.length; i++) {
                            if (listCountry[i].value == e.target.value) {
                                index = i;
                                break;
                            }
                        }
                        $(ele).val(listCountry[index].value);
                    }
                });
        });
    },

    initNationalDropdown: function () {
        var $nationalList = $('select[data-plugin="national"]');
        $.each($nationalList, function (i, ele) {
            var listNational;
            var defaultValue = $(ele).attr("data-default");
            var defaultText = $(ele).attr("data-default-text");

            if (defaultValue !== "" && defaultValue != undefined) {
                $(ele).append('<option value="' + defaultValue + '">' + defaultText + '</option>');
            } else {
                $(ele).append('<option value="SG">SINGAPOREAN</option>');
            }

            $(ele).select2({
                tags: false,
                ajax: {
                    url: "/api/search/national",
                    data: function (params) {
                        var query = {
                            skip: window.Bunny.searchAjaxConfig.skip,
                            take: window.Bunny.searchAjaxConfig.take,
                            terms: params.term
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        listNational = data == undefined ? [] : data.items;
                        return {
                            results: $.map(listNational,
                                function (item) {
                                    return {
                                        text: item.text,
                                        id: item.value
                                    }
                                })
                        };
                    }
                }
            });

            $(ele).on("select2:select",
                function (e) {
                    if (e.target.value === $(this).find(":selected").text()) {
                        $(this).val($(this).find(":selected").text());
                    } else {
                        var index = 0;
                        for (var i = 0; i < listNational.length; i++) {
                            if (listNational[i].value == e.target.value) {
                                index = i;
                                break;
                            }
                        }
                        $(ele).val(listNational[index].value);
                    }
                });
        });
    },

    setupAjax: function () {
        $.ajaxSetup({
            // Remove due to not use anti foreign token
            //headers: { 'X-XSRF-TOKEN': $('[name=ape]').val() },
            type: "POST",
            cache: false,
            error: function (xhr, textStatus, errorThrown) {
                if (window.Bunny.isDebug !== true) {
                    return;
                }
                try {
                    // Check request already abort => return
                    if (xhr.status === 0) {
                        return;
                    }

                    var data = JSON.parse(xhr.responseText);

                    if (data.code) {
                        window.Bunny.notify("Error", data.message, "error");
                    }
                    else {
                        window.Bunny.notify("Error", "System error, please try again or contact administrator!", "error");
                    }
                } catch (e) {
                    window.Bunny.notify("Error", "System error, please try again or contact administrator!", "error");
                }
            }
        });
    },

    notify: function (title, message, type, options) {
        options = options || {};
        title = title || undefined;
        message = message || '';
        type = type || 'info';
        options.progressBar = options.progressBar || true;
        options.timeOut = options.timeOut || 5000;

        options.preventDuplicates = options.preventDuplicates || false;
        options.positionClass = options.positionClass || "toast-bottom-right";
        //options.zIndex = options.zIndex;
        options.preventDuplicates = true;

        switch (type) {
            case 'success':
                toastr.success(message, title, options);
                break;
            case 'warning':
                toastr.warning(message, title, options);
                break;
            case 'error':
                toastr.error(message, title, options);
                break;
            case 'info':
                toastr.info(message, title, options);
                break;
            default:
                toastr.info(message, title, options);
        }
    },

    inBunnylidePanel: function () {
        $('[data-toggle="slidePanel"]').click(function () {
            var $this = $(this);
            $.slidePanel.show({
                url: $this.data("url"),
                settings: {
                    method: 'GET',
                    cache: false
                }
            },
                // Option
                {
                    direction: 'right',
                });
        });
    },

    initAutoRenderSlug: function () {
        $('[data-bind-type="slug"]').on("keyup blur", function () {
            var $this = $(this);

            var $slugElement = $($this.data("bind-to"));

            $slugElement.val($this.val().genSlug());
        });
    },

    abbreviateNumber: function (numberIn) {
        var number = parseInt(numberIn);
        var result = number.toLocaleString();

        return result;
    },

    decodeHtml: function (str) {
        return $("<div/>").html(str).text();
    },

    openLink: function (link, isNewTab) {
        if (isNewTab && isNewTab === true) {
            var win = window.open(link, "_blank");
            win.focus();
        } else {
            window.open(link, "_self");
        }
    },

    addListener_Keyboard_Enter: function (selectorsSource, elementDestination, action) {
        /// <summary>
        ///     Add listener when element source press enter make element destination fire a action
        /// </summary>
        /// <param name="selectorsSource" type="type">seperate "," multiple selector: ".class1,#element1"</param>
        /// <param name="elementDestination" type="type">destination selector</param>
        /// <param name="action" type="type">action: "click", "dbclick" and so on</param>

        var elements = selectorsSource.split(",");
        $.each(elements,
            function (index, element) {
                $(element).keydown(function (e) {
                    if (e.which === 13) {
                        $(elementDestination).trigger(action);
                    }
                });
            });
    },

    formatDateTime: function (dateTime) {
        return moment(dateTime).format(window.constants.dateTimeFormat);
    },

    formatDate: function(date) {
        var monthNames = [
            "01", "02", "03",
            "04", "05", "06", "07",
            "08", "09", "10",
            "11", "12"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        if (day < 10) {
            day = '0' + day;
        }

        return day + '/' + monthNames[monthIndex] + '/' + year;
    },

    initDatePicker: function () {
        //$.fn.datepicker.defaults.autoclose = true;
        //$.fn.datepicker.defaults.orientation = window.constants.datePickerOrientation;
        //$.fn.datepicker.defaults.format = window.constants.dateFormat;
    },

    resetValidateForm: function (formId) {
        try {
            var form = $('#' + formId);
            form.validate().resetForm();
            form.valid();
            event.preventDefault();
        }
        catch (err) {
            //
            event.preventDefault();
        }
    },

    disableValidateForm: function (formId) {
        try {
            $("#" + formId).validate().settings.ignore = "*";
            event.preventDefault();
        }
        catch (err) {
            //
            event.preventDefault();
        }
    },

    enableValidateForm: function (formId) {
        try {
            $("#" + formId).validate().settings.ignore = ":hidden";
            event.preventDefault();
        }
        catch (err) {
            //
            event.preventDefault();
        }
    },

    initDropify: function () {
        var $dropifys = $('[data-plugin="dropify"]');

        $.each($dropifys, function (i, ele) {
            var dropifyEvent = $(ele).dropify();

            var dropifyData = dropifyEvent.data('dropify');

            dropifyData.destroy();

            dropifyData.settings.messages = {
                'default': $(ele).data("message-default") || 'Drag and drop a file here or click',
                'replace': $(ele).data("message-error") || 'Drag and drop or click to replace',
                'remove': $(ele).data("message-remove") || 'Remove',
                'error': $(ele).data("message-replace") || 'Ooops, something wrong happended.'
            }

            dropifyData.settings.tpl = {
                wrap: '<div class="dropify-wrapper"></div>',
                loader: '<div class="dropify-loader"></div>',
                message: '<div class="dropify-message"><span class="file-icon" /> <p>' + dropifyData.settings.messages.default + '</p></div>',
                preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">' + dropifyData.settings.messages.replace + '</p></div></div></div>',
                filename: '<p class="dropify-filename"><span class="dropify-filename-inner"></span></p>',
                clearButton: '<button type="button" class="dropify-clear">' + dropifyData.settings.messages.remove + '</button>',
                errorLine: '<p class="dropify-error">' + dropifyData.settings.messages.error + '</p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }

            dropifyData.init();
        });
    },

    dataTableAmountRender: function (data, type, row) {
        return window.Bunny.abbreviateNumber(data);
    },

    dataTableImageRender: function (data, type, row) {
        return window.Bunny.abbreviateNumber(data);
    },

    dataTableDrawCallBack: function (oSettings) {
        window.Bunny.initToolTip();
        window.Bunny.initConfirmDialog();
    },

    dataTableResponsiveCallBack: function (e, datatable, columns) {
        var count = columns.reduce(function (a, b) {
            return b === false ? a + 1 : a;
        }, 0);

        var $table = $("#" + e.currentTarget.id);

        var totalHeaderRow = $table.find("thead").find("tr");

        var lastHeaderRow = totalHeaderRow[totalHeaderRow.length - 1];

        if (count > 0) {
            $(lastHeaderRow).addClass("hidden");
        } else {
            $(lastHeaderRow).removeClass("hidden");
        }
    },

    GenerateGuid: function () {
        return window.Bunny.s4() + window.Bunny.s4() + '-' + window.Bunny.s4() + '-' + window.Bunny.s4() + '-' + window.Bunny.s4() + '-' + window.Bunny.s4() + window.Bunny.s4() + window.Bunny.s4();
    },

    s4: function () {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },

    initialMenubar: function () {
        if (typeof (Storage) !== "undefined") {
            $("#toggleMenubar").on("click", function () {
                var isCollapsed = $("body").hasClass("site-menubar-fold");

                if (isCollapsed === true) {
                    localStorage.setItem("menu-bar-class", "site-menubar-unfold");
                } else {
                    localStorage.setItem("menu-bar-class", "site-menubar-fold");
                }
            });

            if (localStorage.getItem("menu-bar-class")) {
                $("body").removeClass("site-menubar-fold");

                $("body").removeClass("site-menubar-unfold");

                $("body").addClass(localStorage.getItem("menu-bar-class"));
            }
        } else {
            // Do Nothing
        }
    },

    initMenuTQ: function (id) {
        //$(".main-content-menu-section .nav-link .progress").hide();
        //$(".main-content-menu-section .nav-link .btn-pending-section").hide();
        //$(".main-content-menu-section .nav-link .progress-label").hide();
        //$(".main-content-menu-section .nav-link .notification-comment").hide();

        var urlAPImenu = "/api/remain/" + id;
        $.getJSON(urlAPImenu, function (json) {

            if (json["isSubmittedTQ"] === false) {
                $(".main-content-menu-section .nav-link .btn-pending-section").hide();
                $(".main-content-menu-section .nav-link .notification-comment").hide();
                $.each(json, function (index, values) {


                    var totalTQ = values["actualTotalQuestion"];
                    var totalAnswerTQ = values["actualTotalAnswer"];



                    if (values["actualTotalQuestion"] === 0) {
                        totalTQ = values["totalQuestion"];
                    }

                    if ($(".main-content-menu-section .nav-link." + index).hasClass("active")) {

                        if ($(".main-content-menu-section .nav-link." + index).hasClass("disable-nav-item")) {
                            $(".main-content-menu-section .nav-link." + index + " .progress").hide();
                            $(".main-content-menu-section .nav-link." + index + " .progress-label").hide();
                        } else {
                            window.Bunny.initCountQuestionAnswer(".main-content-scrollbar .box-content-question");
                            $(".main-content-menu-section .nav-link." + index + " .progress").show();
                            $(".main-content-menu-section .nav-link." + index + " .progress-label").show();
                        }
                    } else {

                        if (totalAnswerTQ === -1) {
                            $(".main-content-menu-section .nav-link." + index + " .progress").hide();
                            $(".main-content-menu-section .nav-link." + index + " .progress-label").hide();
                        }
                        else {
                            window.Bunny.initMessagesTQ([totalAnswerTQ, totalTQ], index);
                            $(".main-content-menu-section .nav-link." + index + " .progress").show();
                            $(".main-content-menu-section .nav-link." + index + " .progress-label").show();
                        }
                    }
                });
                //window.Bunny.initHeightTQ();
            } else {
                $(".main-content-menu-section .nav-link .progress").hide();
                $(".main-content-menu-section .nav-link .progress-label").hide();
                $(".main-content-menu-section .nav-link .notification-comment").hide();
                var urlAPIComment = "/api/tq-section-approval/" + id;
                window.Bunny.approvedCommentTQ(urlAPIComment, "", "");

            }
        });
    },

    initMenuTQWRER: function (id, section) {
        $(".main-content-menu-section .nav-item .box-status-section .btn-pending-section").hide();
        $(".main-content-menu-section .nav-link .notification-comment").hide();

        if (section != undefined) {
            var urlAPImenu = "/api/remain/" + id;
            $.getJSON(urlAPImenu, function (json) {
                window.Bunny.menuTQWR(id, json, "ER");
                //window.Bunny.initHeightTQ("ER");
            });
        }
    },

    initMenuTQWREE: function (id, section) {
        $(".main-content-menu-section .nav-item .box-status-section .btn-pending-section").hide();
        $(".main-content-menu-section .nav-link .notification-comment").hide();

        if (section != undefined) {
            var urlAPImenu = "/api/remain/" + id;
            $.getJSON(urlAPImenu, function (json) {
                window.Bunny.menuTQWR(id, json, "ITR");
                //window.Bunny.initHeightTQ("ITR");
            });
        }
    },

    menuTQWR: function (id, json, type) {
        var urlAPIComment = "/api/tq-section-approval/" + id;

        if (json["isSubmittedTQ"] === false) {
            //window.Bunny.initHeightTQ();
            $.getJSON(urlAPIComment, function (json) {
                window.Bunny.checkSubmitRevertButton(json);
            });
            $(".main-TQ-work-record .main-content-menu-section.TQ-work-record-" + type + " .nav-item .status-section").addClass("not-submit");

        } else {
            var classMenu = ".TQ-work-record-" + type;
            window.Bunny.approvedCommentTQWR(urlAPIComment, ".main-TQ-work-record", classMenu);

        }
    },

    setCookieSubmit: function (cname, cvalue, exdays) {
        document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },

    submBunnyection: function (tqId) {


        var menuTaxQuestionnaire = $(".main-content-menu-section ul li");

        menuTaxQuestionnaire.each(function () {

            if (menuTaxQuestionnaire.children().hasClass("active")) {

                //set cookies
                window.Bunny.setCookieSubmit("IsAutoSubmitTQ", true, 1);

                // click save

                var idSubmit = menuTaxQuestionnaire.children(".active").attr("data-form-id");
                var idButtonSave = $("#" + idSubmit + " .box-action button:first-child").attr("id");

                if (idSubmit === "section2Form") {
                    $("#submit_section2").click();
                    //window.Bunny.callBackSubmit(tqId);
                } else if (idSubmit === "section10Form") {
                    //var callbackSubmit = true;
                    Submit();
                } else {
                    $('#' + idButtonSave).click();
                    $('#' + idSubmit).submit();
                    //window.Bunny.callBackSubmit(tqId);
                }
                return false;
            }

        });

        //var url = "/portal/taxquestionnaire/submit-section";
        //var result = {
        //    TaxQuestionnaireId: tqId,
        //};

        //$.ajax
        //    ({
        //        type: "post",
        //        url: url,
        //        data: JSON.stringify(result),
        //        dataType: "json",
        //        contentType: "application/json; charset=utf-8",
        //        success: function (data) {
        //            Bunny.notify("Submit Section Success", "Submit Section successful", "success");
        //            window.location.reload();
        //        },
        //        error: function () {
        //            Bunny.notify("Submit Section Fail", "Submit Section fail, please try again", "error");
        //        }
        //    });
    },

    callBackSubmit: function (tqId) {
        var url = "/portal/taxquestionnaire/submit-section";
        var result = {
            TaxQuestionnaireId: tqId
        };

        $.ajax
            ({
                type: "post",
                url: url,
                data: JSON.stringify(result),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    Bunny.notify("Submit Section Success", "Submit Section successful", "success");
                    window.location.reload();
                },
                error: function (err) {
                    Bunny.notify("Submit Section Fail", "Submit Section fail, please try again", "error");
                }
            });
    },

    approvedCommentTQ: function (urlAPIComment, str, classMenu) {
        $.getJSON(urlAPIComment, function (json) {
            window.Bunny.checkSubmitRevertButton(json);
            $.each(json, function (index, values) {
                var section = values["section"].toLowerCase();
                if ($(str + " .main-content-menu-section" + classMenu + " .nav-link." + section).hasClass("disable-nav-item")) {
                    $(str + " .main-content-menu-section" + classMenu + " .nav-link." + section + " .btn-pending-section").hide();
                    $(str + " .main-content-menu-section" + classMenu + " .nav-link." + section + " .progress").hide();
                } else {
                    if (values["isApproved"] === true) {
                        $(str + " .main-content-menu-section" + classMenu + " .nav-link." + section + " .btn-pending-section").addClass("btn-approved-section");
                    } else {
                        $(str + " .main-content-menu-section" + classMenu + " .nav-link." + section + " .btn-pending-section").removeClass("btn-approved-section");
                    }
                }

                if (values["totalComments"] !== 0) {
                    $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").show();
                    $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment .number-comment").text(values["totalComments"]);

                    var intComment = parseInt(values["totalComments"]) - parseInt(values["doneComments"]);
                    if (intComment === 0) {
                        $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").addClass("comment-done");
                    } else {
                        $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").removeClass("comment-done");
                    }
                }
            });
        });

        $(".main-content-menu-section" + classMenu + " .nav-link .btn-pending-section").show();
        //window.Bunny.initHeightTQ();

    },

    approvedCommentTQWR: function (urlAPIComment, str, classMenu) {
        $.getJSON(urlAPIComment, function (json) {
            window.Bunny.checkSubmitRevertButton(json);
            $.each(json, function (index, values) {
                var section = values["section"].toLowerCase();
                if ($(str + " .main-content-menu-section" + classMenu + " .nav-item." + section).hasClass("disable-nav-item")) {
                    $(str + " .main-content-menu-section" + classMenu + " .nav-item." + section + " .progress").hide();
                }
                if (values["isApproved"] === true) {
                    $(str + " .main-content-menu-section" + classMenu + " .nav-item." + section + " .btn-pending-section").addClass("btn-approved-section");
                } else {
                    $(str + " .main-content-menu-section" + classMenu + " .nav-item." + section + " .btn-pending-section").removeClass("btn-approved-section");
                }


                if (values["totalComments"] !== 0) {
                    $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").show();
                    $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment .number-comment").text(values["totalComments"]);

                    var intComment = parseInt(values["totalComments"]) - parseInt(values["doneComments"]);
                    if (intComment === 0) {
                        $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").addClass("comment-done");
                    } else {
                        $(".main-content-menu-section" + classMenu + " .nav-link." + section + " .notification-comment").removeClass("comment-done");
                    }
                }
            });
        });

        $(".main-content-menu-section" + classMenu + " .nav-item .box-status-section .btn-pending-section").show();
        $(".main-content-menu-section" + classMenu + " .nav-item .box-status-section").show();
        //window.Bunny.initHeightTQ();

    },

    checkSubmitRevertButton: function (data) {

        var isShowUnsubmit = true;
        $.each(data, function (index, item) {
            var commentsNum = item.totalComments;
            var isApproveSection = item.isApproved;
            if (commentsNum > 0) {
                isShowUnsubmit = false;
                return;
            }

            if (isApproveSection) {
                isShowUnsubmit = false;
                return;
            }
        });

        if (isShowUnsubmit) {
            $(".btn-revert-tax-questionnaire").remove();
            $(".btn-unsubmit-tax-questionnaire").removeClass("dis-none");
        } else {
            $(".btn-revert-tax-questionnaire").removeClass("dis-none");
            $(".btn-unsubmit-tax-questionnaire").remove();
        }
    },

    changeStatuApprovedComment: function (id, section, prefix) {
        section = section.replace("S", "s");
        var index = parseInt(section.replace("section", "")) - 1;
        if (prefix == undefined || prefix === '') {
            prefix = "";
        }

        var urlAPIComment = "/api/tq-section-approval/" + id;

        $.getJSON(urlAPIComment, function (json) {
            if (json[index]["isApproved"] === true) {
                $(".menu-section-tq .nav-link." + section + " .btn-pending-section").addClass("btn-approved-section");
                $("#" + prefix + "submit_" + section).hide();
                $("." + prefix + "box-comment-" + section + " .box-add-new-comment").hide();
                $("." + prefix + "box-comment-" + section + " .list-box-comment .user-comment-tq").each(function (i, ele) {
                    $(ele).find(".content-current-box-comment .box-action-user-comment").addClass("disable-action-comment");
                    $(ele).find(".content-current-box-comment .box-action-user-comment input").attr('disabled', 'disabled');
                });
            } else {
                $(".menu-section-tq .nav-link." + section + " .btn-pending-section").removeClass("btn-approved-section");
                $("#" + prefix + "submit_" + section).show();
                $("." + prefix + "box-comment-" + section + " .box-add-new-comment").show();
                $("." + prefix + "box-comment-" + section + " .list-box-comment .user-comment-tq").each(function (i, ele) {
                    $(ele).find(".content-current-box-comment .box-action-user-comment").removeClass("disable-action-comment");
                    $(ele).find(".content-current-box-comment .box-action-user-comment input").removeAttr('disabled');
                });
            }

            if (json[index]["totalComments"] !== 0) {
                $(".menu-section-tq .nav-link." + section + " .notification-comment").show();
                $(".menu-section-tq .nav-link." + section + " .notification-comment .number-comment").text(json[index]["totalComments"]);

                var intComment = parseInt(json[index]["totalComments"]) - parseInt(json[index]["doneComments"]);
                if (intComment === 0) {
                    $(".menu-section-tq .nav-link." + section + " .notification-comment").addClass("comment-done");
                } else {
                    $(".menu-section-tq .nav-link." + section + " .notification-comment").removeClass("comment-done");
                }
            }
        });
    },

    checkApprovedAllSection: function (id, prefix) {
        var urlApproved = "/api/check-approved/is-approved/" + id;
        $.getJSON(urlApproved, function (json) {
            if (json.isApprovedITR === true && json.isApprovedER === true) {

                $(".header-tab-ITR .btn-approved-answers").text(json.statusITR);
                $(".header-tab-ER .btn-approved-answers").text(json.statusER);
                $(".header-tab-ITR .btn-approved-answers").addClass("approved");
                $(".header-tab-ER .btn-approved-answers").addClass("approved");

                $(".btn-approved-answers.btn-approved-itr").text("ITR - " + json.statusITR);
                $(".btn-approved-answers.btn-approved-itr").addClass("approved");
                $(".btn-approved-answers.btn-approved-er").text("ER - " + json.statusER);
                $(".btn-approved-answers.btn-approved-er").addClass("approved");

                $("#taxComputationInfoTab #startComputation").removeClass("dis-none");
                $("#taxComputationInfoTab #startComputation").parent().removeClass("dis-none");

                $("#ItrTaxQuestionaireInfoTab #btn-submit-ee").hide();
                $("#ErTaxQuestionaireInfoTab #btn-submit-er").hide();
            } else if (json.isApprovedITR === true) {

                $(".header-tab-ITR .btn-approved-answers").text(json.statusITR);
                $(".header-tab-ITR .btn-approved-answers").addClass("approved");

                $(".header-tab-ER .btn-approved-answers").text(json.statusER);

                $(".btn-approved-answers.btn-approved-itr").text("ITR - " + json.statusITR);
                $(".btn-approved-answers.btn-approved-itr").addClass("approved");
                $(".btn-approved-answers.btn-approved-er").text("ER - " + json.statusER);

                $("#taxComputationInfoTab #startComputation").addClass("dis-none");
                $("#taxComputationInfoTab #startComputation").parent().addClass("dis-none");

                $("#ItrTaxQuestionaireInfoTab #btn-submit-ee").hide();
                $("#ErTaxQuestionaireInfoTab #btn-submit-er").show();
            } else if (json.isApprovedER === true) {
                $(".header-tab-ER .btn-approved-answers").text(json.statusER);
                $(".header-tab-ER .btn-approved-answers").addClass("approved");
                $(".header-tab-ITR .btn-approved-answers").text(json.statusITR);

                $(".btn-approved-answers.btn-approved-itr").text("ITR - " + json.statusITR);
                $(".btn-approved-answers.btn-approved-er").text("ER - " + json.statusER);
                $(".btn-approved-answers.btn-approved-er").addClass("approved");

                $("#taxComputationInfoTab #startComputation").addClass("dis-none");
                $("#taxComputationInfoTab #startComputation").parent().addClass("dis-none");

                $("#ItrTaxQuestionaireInfoTab #btn-submit-ee").show();
                $("#ErTaxQuestionaireInfoTab #btn-submit-er").hide();
            } else {
                $(".header-tab-ITR .btn-approved-answers").text(json.statusITR);
                $(".header-tab-ER .btn-approved-answers").text(json.statusER);

                $(".btn-approved-answers.btn-approved-itr").text("ITR - " + json.statusITR);
                $(".btn-approved-answers.btn-approved-er").text("ER - " + json.statusER);

                $("#taxComputationInfoTab #startComputation").addClass("dis-none");
                $("#taxComputationInfoTab #startComputation").parent().addClass("dis-none");

                $("#ItrTaxQuestionaireInfoTab #btn-submit-ee").show();
                $("#ErTaxQuestionaireInfoTab #btn-submit-er").show();
            }
        });
    },

    initCountQuestionAnswer: function (className) {
        var sumQuestion = 0;
        var count = 0;
        var nameInput = [];
        var nameAmount = [];

        $(className + " :input").each(function (index, ele) {
            var inputNone = 0;

            if ($(ele).hasClass("v-hidden") || $(ele).hasClass("dis-none") || $(ele).attr("type") === "hidden"
                || $(ele).attr("hidden") === "hidden" || $(ele).is('[readonly]')) {
                inputNone = 1;
            }

            if ($(ele).is("select")) {
                if ($(ele).parent().hasClass("dis-none")) {
                    inputNone = 1;
                }
            }

            if (inputNone === 0) {

                if ($(ele).is('input:text') && $(ele).attr("data-plugin") !== "decimal-format") {
                    if ($(ele).parent().hasClass("has-success") || $(ele).val() !== "") {
                        count += 1;
                    }
                    sumQuestion += 1;
                }

                if ($(ele).is('input:text') && $(ele).attr("data-plugin") === "decimal-format") {
                    if ($.inArray($(this).attr("name"), nameAmount) === -1) {
                        nameAmount.push($(this).attr("name"));
                    }
                }

                if ($(ele).is("select")) {
                    count += 1;
                    sumQuestion += 1;
                }

                if ($(ele).attr("type") === "checkbox") {
                    if ($(ele).is(":checked")) {
                        count += 1;
                    }
                    sumQuestion += 1;
                }

                if ($(ele).attr("type") === "radio") {
                    if ($(ele).is(":checked")) {
                        count += 1;
                    } else {
                        if ($.inArray($(this).attr("name"), nameInput) === -1) {
                            nameInput.push($(this).attr("name"));
                        }
                    }
                }

                if ($(ele).attr("type") === "file") {
                    count += 1;
                    sumQuestion += 1;
                }
            }
        });
        count += nameAmount.length;
        sumQuestion += nameAmount.length;

        sumQuestion += nameInput.length;

        $("#ActualTotalQuestion").val(sumQuestion);
        $("#ActualTotalAnswer").val(count);

        window.Bunny.initMessagesTQ([count, sumQuestion], "");
    },

    initMessagesTQ: function (result, index) {
        if (index !== "") {
            if ((result[0] / result[1]) === 1 || result[0] === 0 && result[1] === 0) {
                if ($(".menu-section-tq ." + index + " .progress .progress-bar").hasClass("progress-bar-warning")) {
                    $(".menu-section-tq ." + index + " .progress .progress-bar").addClass("progress-bar-success");
                    $(".menu-section-tq ." + index + " .progress .progress-bar").removeClass("progress-bar-warning");
                } else {
                    $(".menu-section-tq li a.active .progress .progress-bar").removeClass("progress-bar-warning");
                }

                if ($(".menu-section-tq ." + index + " .contextual-progress .progress-label").hasClass("text-warning")) {
                    $(".menu-section-tq ." + index + " .contextual-progress .progress-label").removeClass("text-warning");
                    $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-success");
                }
                else {
                    $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-success");
                }

                $(".menu-section-tq ." + index + " .progress .progress-bar").css("width", "100%");
                $(".menu-section-tq ." + index + " .contextual-progress .progress-label").text("Done!");
            }
            else {
                if (result[1] != 0) {
                    var persen = (result[0] / result[1]) * 100;

                    if ($(".menu-section-tq ." + index + " .progress .progress-bar").hasClass("progress-bar-success")) {
                        $(".menu-section-tq ." + index + " .progress .progress-bar").removeClass("progress-bar-success");
                        $(".menu-section-tq ." + index + " .progress .progress-bar").addClass("progress-bar-warning");
                    } else {
                        $(".menu-section-tq ." + index + " .progress .progress-bar").addClass("progress-bar-warning");
                    }

                    if ($(".menu-section-tq ." + index + " .contextual-progress .progress-label").hasClass("text-success")) {
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").removeClass("text-success");
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-warning");
                    }
                    else {
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-warning");
                    }

                    $(".menu-section-tq ." + index + " .progress .progress-bar").css("width", persen + "%");
                    $(".menu-section-tq ." + index + " .contextual-progress .progress-label").text(result[0] + '/' + result[1]);
                }
                else {
                    if ($(".menu-section-tq ." + index + " .progress .progress-bar").hasClass("progress-bar-success")) {
                        $(".menu-section-tq ." + index + " .progress .progress-bar").removeClass("progress-bar-success");
                        $(".menu-section-tq ." + index + " .progress .progress-bar").addClass("progress-bar-warning");
                    }
                    else {
                        $(".menu-section-tq ." + index + " .progress .progress-bar").addClass("progress-bar-warning");
                    }

                    if ($(".menu-section-tq ." + index + " .contextual-progress .progress-label").hasClass("text-success")) {
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").removeClass("text-success");
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-warning");
                    }
                    else {
                        $(".menu-section-tq ." + index + " .contextual-progress .progress-label").addClass("text-warning");
                    }

                    $(".menu-section-tq ." + index + " .progress .progress-bar").css("width", "0%");
                    $(".menu-section-tq ." + index + " .contextual-progress .progress-label").text(result[0] + '/' + result[1]);
                }
            }
        }
        else {
            if ((result[0] / result[1]) == 1 || result[0] === 0 && result[1] === 0) {
                $(".menu-section-tq li a.active .progress-label").text("Done!");
                if ($(".menu-section-tq li a.active .progress .progress-bar").hasClass("progress-bar-warning")) {
                    $(".menu-section-tq li a.active .progress .progress-bar").addClass("progress-bar-success");
                    $(".menu-section-tq li a.active .progress .progress-bar").removeClass("progress-bar-warning");
                } else {
                    $(".menu-section-tq li a.active .progress .progress-bar").removeClass("progress-bar-warning");
                }

                if ($(".menu-section-tq li a.active .progress-label").hasClass("text-warning")) {
                    $(".menu-section-tq li a.active .progress-label").removeClass("text-warning");
                    $(".menu-section-tq li a.active .progress-label").addClass("text-success");
                } else {
                    $(".menu-section-tq li a.active .progress-label").addClass("text-success");
                }

                $(".menu-section-tq li a.active .progress .progress-bar").css("width", "100%");
            }
            else {
                $(".menu-section-tq li a.active .progress-label").text(result[0] + '/' + result[1]);
                var persen = (result[0] / result[1]) * 100;

                if ($(".menu-section-tq li a.active .progress .progress-bar").hasClass("progress-bar-success")) {
                    $(".menu-section-tq li a.active .progress .progress-bar").removeClass("progress-bar-success");
                    $(".menu-section-tq li a.active .progress .progress-bar").addClass("progress-bar-warning");
                } else {
                    $(".menu-section-tq li a.active .progress .progress-bar").addClass("progress-bar-warning");
                }

                if ($(".menu-section-tq li a.active .progress-label").hasClass("text-success")) {
                    $(".menu-section-tq li a.active .progress-label").removeClass("text-success");
                    $(".menu-section-tq li a.active .progress-label").addClass("text-warning");
                } else {
                    $(".menu-section-tq li a.active .progress-label").addClass("text-warning");
                }

                $(".menu-section-tq li a.active .progress .progress-bar").css("width", persen + "%");
            }
        }
    },

    inBunnyubmitFormTQ: function (idSubmitForm) {

        $(".main-content-menu-section .menu-section-tq .nav-item .nav-link").click(function () {

            if (!window["isFromWorkRecord"]) {
                var linkRedirect = $(this).attr("data-href");
                $("#RedirecLink").val(linkRedirect);
                if ($(".main-content-menu-section .menu-section-tq .nav-item .nav-link.active").hasClass("disable-nav-item")) {
                    window.location.href = linkRedirect;
                } else {
                    if (idSubmitForm === '#submit_section2') {
                        SaveAll();
                    } else if (idSubmitForm === '#ITRsubmit_section2') {
                        ITRSaveAll();
                    } else {
                        $(idSubmitForm).trigger("click");
                    }
                }
            } else {
                window["CurrentSectionActive"] = undefined;
                if (idSubmitForm != undefined) {

                    var linkRedirect = $(this).attr("data-href");

                    if (linkRedirect.indexOf('sectionEr') !== -1) {
                        window["NextSectionER"] = linkRedirect.split("sectionEr=")[1].charAt(0);
                    }

                    if (linkRedirect.indexOf('sectionItr') !== -1) {
                        window["NextSectionITR"] = linkRedirect.split("sectionItr=")[1].substring(0, 2);
                        if (window["NextSectionITR"].indexOf('&') !== -1) {
                            window["NextSectionITR"] = window["NextSectionITR"].charAt(0);
                        }
                    }

                    if ($(idSubmitForm).is(":visible")) {

                        if (idSubmitForm === '#submit_section2') {
                            SaveAll();
                        } else if (idSubmitForm === '#ITRsubmit_section2') {
                            ITRSaveAll();
                        } else {
                            $(idSubmitForm).trigger("click");
                        }
                    } else {
                        window.location.href = linkRedirect;
                    }
                    // window.location.href = linkRedirect;
                }
            }
        });
    },

    inBunnyhowHideData: function (object) {
        $.each(object, function (index, value) {
            var idForm = $("[data-show-hide=" + value.show + "]").parents("form");
            var result = window.Bunny.initCheckCondition(value.condition, idForm);
            if (result === true) {
                idForm.find("[data-show-hide=" + value.show + "]").removeClass("dis-none");
            } else {
                idForm.find("[data-show-hide=" + value.show + "]").addClass("dis-none");
            }
        });
    },

    initCheckCondition: function (array, idForm) {
        var resultCheck = [];
        var result = true;
        if (idForm == undefined) return false;
        $.each(array, function (index, val) {
            if (val.select === "select") {
                var current = idForm.find("select[name=" + val.name + "]");
                var valSelect = current.find(":selected").val();

                if (Array.isArray(val.value) === true) {
                    if ($.inArray(valSelect, val.value) === -1) {
                        //result = false;
                        resultCheck.push(false);
                        //return false;
                    } else {
                        resultCheck.push(true);
                    }
                } else {
                    if (valSelect !== val.value) {
                        //result = false;
                        //return false;
                        resultCheck.push(false);
                    } else {
                        resultCheck.push(true);
                    }
                }
            } else {
                var valinput = idForm.find("input[name=" + val.name + "]:checked").val();
                if (valinput !== val.value) {
                    //result = false;
                    //return false;
                    resultCheck.push(false);
                } else {
                    resultCheck.push(true);
                }
            }
        });

        return resultCheck.indexOf(true) > -1;
    },

    initRenderTemplateHTML: function (template, data) {
        $.each(data, function (index, value) {
            if (index === "placeholder") {
                template = template.replace("{{placeholder}}", value);
            } else {
                template = template.replace("{{placeholder}}", "Name");
                template = template.replace("{{" + index + "}}", value);
            }

        });
        return template;
    },

    initcheckLabelAlreadyExist: function (str, label) {
        var result = true;
        $(str).each(function () {
            var value = $(this).find("label").text();
            if (label === value) {
                result = false;
            }
        });
        return result;
    },

    initAutoDecimalFormat: function () {
        var $amounts = $('[data-plugin="decimal-format"]');

        $.each($amounts, function (i, ele) {
            $(ele).inputmask({
                alias: "decimal",
                skipRadixDance: true,
                groupSeparator: ".",
                placeholder: "0",
                autoGroup: true,
                rightAlign: false,
                digBunny: 2,
                digBunnyOptional: false,
                //clearMaskOnLostFocus: 
            });
        });

        //$.each($amounts, function (i, ele) {
        //    $(ele).inputmask({
        //        mask: "/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:(\.|,)\d+)?$/",
        //        rightAlign: false,
        //    });

        //});
    },
    initValidationModal: function () {
        var $validation = $('.modal.show [data-valid="validation-modal"]');
        var result = true;

        $.each($validation, function (i, ele) {
            var divError = $(ele).parent().find("span");
            if ($(ele).val() === "") {
                var label = $(ele).parent().find("label").text().split("*")[0].trim();
                if (!divError.hasClass("error-message")) {
                    $(ele).parent().append("<span class='d-block error-message' style='width: 100%;'>" + label + " is required");
                }
                result = false;
            } else {
                $(ele).parent().find(".error-message").remove();
            }
        });
        //$(".loader-overlay").remove();
        return result;
    },

    initChangeValidationModal: function () {
        var $validation = $('[data-valid="validation-modal"]');
        var result = true;
        $.each($validation, function (i, ele) {
            $(ele).on('change', function () {
                window.Bunny.initValidationModal();
            });
        });
    },

    initDefaultValidationModal: function () {
        var $link = $('button[data-valid="add-row"]');
 
        $.each($link, function (i, ele) {
            $(ele).on('click', function (event) {
                var callBack = $(ele).attr("data-call-back");
                var rs = window.Bunny.initValidationModal();
                if (rs) {
                    window[callBack]();
                }
            });
        });
    },

    //initHeightTQ: function (str) {
    //    if (str) {
    //        var h = $(".main-TQ-work-record .main-content-tax-questionaire.main-tax-questionnaire-" + str + " .main-content-section").height();
    //        $(".main-TQ-work-record .main-content-menu-section.TQ-work-record-" + str).css("height", (h + 30));
    //    } else {
    //        var h = $(".main-content-tax-questionaire .main-content-section").height();

    //        var hmenu = $(".main-content-tax-questionaire .main-content-menu-section .menu-section-tq").height();

    //        if (hmenu > h) {
    //            $(".main-content-tax-questionaire .main-content-menu-section").css("height", (h + 30));
    //        } else {
    //            $(".main-content-tax-questionaire .main-content-menu-section").css("height", "100%");
    //            $(".main-content-tax-questionaire .main-content-section").css("border-left", "1px solid #e0e0e0");
    //            $(".main-content-tax-questionaire .main-content-menu-section").css("border-right", "none");
    //        }
    //    }
    //},

    inBunnyummernote: function () {
        var $summernotes = $('[data-plugin="summernote"]');

        $.each($summernotes,
            function (i, ele) {
                var $ele = $(ele);

                $ele.summernote('destroy');

                var height = $ele.data("summernote-height") || 400;

                var callbacks = $ele.data("summernote-callbacks") || {};

                $ele.summernote({
                    height: height,
                    codemirror: {
                        theme: 'monokai'
                    },
                    toolbar: [
                        // https://summernote.org/deep-dive/#custom-toolbar-popover
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph', 'style', 'height']],
                        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
                        ['utils', ['codeview', 'undo', 'redo']]
                    ],
                    callbacks: callbacks
                });
            });
    },

    initLoading: function () {
        $("body").animsition({
            inClass: 'fade-in',
            inDuration: 800,
            loading: true,
            loadingClass: 'loader-overlay',
            loadingParentElement: 'html',
            loadingInner: '\n      <div class="loader-content">\n        <div class="loader-index">\n          <div></div>\n          <div></div>\n          <div></div>\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>',
            onLoadEvent: true
        });
    },
    initLoadingFormSubmit: function () {
        window.Bunny.initLoading();
        return true;
    },

    reloadWorkRecord: function (section) {

        var activeTab = $(".tab-work-record a.active").attr("href");
        var isViewEr = 'False';
        var sectionEr = 1;
        var sectionItr = 1;
        var url = window.location.href;
       

        if (activeTab === '#ItrTaxQuestionaireInfoTab') {
            //reload itr tab
            if (section != undefined) {
             
                sectionItr = section.replace("section", "");
            } else {
                if (window["NextSectionITR"] != undefined) {
                    sectionItr = window["NextSectionITR"];
                } else if (url.indexOf('sectionItr') !== -1) {
                    sectionItr = url.split("sectionItr=")[1].substring(0, 2);
                    if (sectionItr.indexOf('&') !== -1) {
                        sectionItr = sectionItr.charAt(0);
                    }
                }
            }

            if (url.indexOf('sectionEr') !== -1) {
                sectionEr = url.split("sectionEr=")[1].charAt(0);
            }

            if (window["NextSectionER"] != undefined) {
                sectionEr = window["NextSectionER"];
            }

        } else {
            //reload er tab
            if (section != undefined) {
                sectionEr = section.replace("section", "");
            } else {
                if (window["NextSectionER"] != undefined) {
                    sectionEr = window["NextSectionER"];
                } else if (url.indexOf('sectionEr') !== -1) {
                    sectionEr = url.split("sectionEr=")[1].substring(0, 2);
                    if (sectionEr.indexOf('&') !== -1) {
                        sectionEr = sectionEr.charAt(0);
                    }
                }
            }

            isViewEr = 'True';
            if (url.indexOf('sectionItr') !== -1) {
                sectionItr = url.split("sectionItr=")[1].substring(0, 2);
                if (sectionItr.indexOf('&') !== -1) {
                    sectionItr = sectionItr.charAt(0);
                }
            }


            if (window["NextSectionITR"] != undefined) {
                sectionItr = window["NextSectionITR"];
            }
        }
        if (url.indexOf('sectionItr') === -1) {
            url += '/section?sectionEr=2&sectionItr=1&isViewER=True';
        }
        var newUrl = window.Bunny.replaceParam(url, "sectionEr", sectionEr);
        newUrl = window.Bunny.replaceParam(newUrl, "sectionItr", sectionItr);
        newUrl = window.Bunny.replaceParam(newUrl, "isViewER", isViewEr);

        window.location.href = newUrl;
    },

    keyPressHandler: function() {
        $(document).keyup(function (e) {
            if (e.keyCode === 27) {

                $('.modal-dialog').each(function (i, modal) {
                    var isShow = $(modal).is(':visible');
                    if (isShow) {
                        var button = $(modal).find('button[id]').not(".hidden").not(".btn_close");
                        $('#' + button[0].id).trigger("click");
                        return;
                    }
                });
            }
        });
    },

    replaceParam: function (url, paramName, paramValue) {
        if (paramValue == null) {
            paramValue = '';
        }
        var pattern = new RegExp('\\b(' + paramName + '=).*?(&|$)');
        if (url.search(pattern) >= 0) {
            return url.replace(pattern, '$1' + paramValue + '$2');
        }
        url = url.replace(/\?$/, '');
        return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue;
    },

    activeSection: function (sender, id, section) {
        $(sender).prop('onclick', null).off('click');

        $.ajax
            ({
                type: "post",
                url: '/api/setup/' + id + '/active?section=section' + section,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data === true) {
                        Bunny.notify("Disable Section Success", "Disable Section " + section + " successful", "success");
                    } else if (data === false) {
                        Bunny.notify("Enable Section Success", "Enable Section " + section + " successful", "success");
                    }
                    else if (data === "InTaxComputation") {
                        Bunny.notify("Enable/Disable Section Fail", "Enable/Disable Section " + section + " fail, In Tax Computation", "error");
                    }
                    //window.Bunny.reloadWorkRecord(section.toString());
                    setTimeout(function () { window.Bunny.reloadWorkRecord(section.toString()); }, 500);

                },
                error: function () {
                    Bunny.notify("Enable/Disable Section Fail", "Enable/Disable Section " + section + " fail, please try again", "error");
                }
            });



    },

    activeSectionContent: function (sender, id, section, field, desc) {
        $(sender).prop('onclick', null).off('click');

        $.ajax
            ({
                type: "post",
                url: '/api/setup/' + id + '/active/content?field=' + field,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data) {
                        Bunny.notify("Disable Success", "Disable " + desc + " successful", "success");
                    } else {
                        Bunny.notify("Enable Success", "Enable " + desc + " successful", "success");
                    }
                    //window.Bunny.reloadWorkRecord(section.toString());
                    setTimeout(function () {
                        //window.Bunny.reloadWorkRecord(section.toString());
                        window.location.href = window.location.href.split('#')[0] + "#" + field;
                        window.location.reload();
                    }, 500);

                },
                error: function () {
                    Bunny.notify("Enable/Disable Fail", "Enable/Disable " + desc + " fail, please try again", "error");
                }
            });
    },

    scrollFocusWithId: function () {

        var url = window.location.href;
        var id = url.split('#')[1];
        var timeInterval = 700;

        //var urlLink = new URL(url);

        // fix IE new URL(url) don't work
        var isShowEr = getQueryString("isViewER");
      
        if (id != undefined) {
            setTimeout(function () {
                var heightPanel = $(".panel.panel-er").offset().top;
                var mainScroll = $(".tab-pane.active .main-content-scrollbar")[0];
                if (isShowEr == 'false' || isShowEr == 'False') {
                    heightPanel = $(".panel.panel-itr").offset().top;
                    mainScroll = $(".tab-pane.active .main-content-scrollbar")[1];
                }

                //var heightPanel = $(".panel").height() + 300;
                $("html").scrollTop(heightPanel + 110);
                var value = $(mainScroll).scrollTop() - $(mainScroll).offset().top + $("#" + id).offset().top - 200;
                $(".main-content-scrollbar").scrollTop(value);

            }, timeInterval);

        } else {

           

            if (isShowEr == 'true' || isShowEr == 'True') {
                setTimeout(function () {
                    if ($(".panel.panel-er").length > 0) {
                        var heightPanel = $(".panel.panel-er").offset().top;
                        if (heightPanel > 0) {
                            var sectionEr = getQueryString("sectionEr");
                            heightPanel = heightPanel + 130;
                            $("html").scrollTop(heightPanel);
                            var value = Number(sectionEr - 1) * 50;
                            $(".main-content-menu-section.TQ-work-record-ER").scrollTop(value);
                        }
                    }
                }, timeInterval);
            } else if (isShowEr == 'false' || isShowEr == 'False') {
                setTimeout(function () {
                   if ($(".panel.panel-itr").length > 0) {
                        var heightPanel = $(".panel.panel-itr").offset().top;
                        if (heightPanel > 0) {
                            var sectionItr = getQueryString("sectionItr");
                            heightPanel = heightPanel + 130;
                            $("html").scrollTop(heightPanel);
                            var value = Number(sectionItr - 1) * 50;
                            $(".main-content-menu-section.TQ-work-record-ITR").scrollTop(value);
                        }
                   }
                }, timeInterval);
            } else {
               
                if (!window["isFromWorkRecord"]) {
                    var sectionCur = url.split('/')[url.split('/').length - 1];
                    var value = Number(sectionCur.replace("section", "") - 1) * 50;

                    setTimeout(function () {
                        $(".main-content-menu-section").scrollTop(value);
                    }, timeInterval);
                }
            }
        }
    },
    changeTabActive: function (tabView) {

        var url = window.location.href;
        if (tabView === 'EDIT') {
            if (url.indexOf('sectionItr') > -1) {
                url = url.split('section?')[0].slice(0, -1);
            }

            if (url.indexOf('taxcomputation') > -1) {
                url = url.replace('/taxcomputation', '');
            }

        } else if (tabView === 'ER') {
            if (url.indexOf('sectionItr') === -1) {
                url += '/section?sectionEr=2&sectionItr=1&isViewER=True';
            }

            if (url.indexOf('taxcomputation') > -1) {
                url = url.replace('/taxcomputation', '');
            }

            url = window.Bunny.replaceParam(url, "isViewER", true);
        } else if (tabView === 'ITR') {
            if (url.indexOf('sectionItr') === -1) {
                url += '/section?sectionEr=2&sectionItr=1&isViewER=True';
            }

            if (url.indexOf('taxcomputation') > -1) {
                url = url.replace('/taxcomputation', '');
            }

            url = window.Bunny.replaceParam(url, "isViewER", false);
        } else if (tabView === 'TAX') {
            if (url.indexOf('section?') > -1) {
                url = url.split('section?')[0].slice(0, -1) + "/taxcomputation";
            } else {
                url += "/taxcomputation";
            }
        }

        history.pushState(null, null, url);
    },

    checkPostalCode: function (elementId, spanError, spanforid) {

        var selector = 'span[data-valmsg-for =\'' + spanError + '\']';
        if (spanforid) {
            selector = ' <span class="d-block error-message" style="width: 100%;" style="color:red;" id="' + spanforid + '">Postal code must 6 digBunny exactly</span>';
        }

        var code = $('#' + elementId).val();
        if (code.length !== 6 && code.length > 0) {
            if (spanforid) {
                $('#' + elementId).after(selector);
            } else {
                $(selector).css("color", "red");
                $(selector).text("Postal code must 6 digBunny exactly");
            }

            return false;
        } else {
            if (spanforid) {
                $(selector).remove();
            } else {
                $(selector).text("");
            }

            return true;
        }
        return true;
    },
    initLadda: function () {
        //Ladda.bind('[data-plugin="ladda"]', { timeout: 5000 });
    },
    IsvalidDateFromDateTo: function (dateFrom, dateTo) {

        var dateFromValue = new Date(dateFrom[2], dateFrom[1] - 1, dateFrom[0]);
        var dateToValue = new Date(dateTo[2], dateTo[1] - 1, dateTo[0]);

        if (dateFromValue >= dateToValue) {
            Bunny.notify("error", "Period invalid", "error");
            return false;
        }
        return true;
    },
    dataCallBackComment: function (arrayComment, arrayDoneComment, formSelector, url, isNotReload) {
        window.Bunny.saveDoneCommentSection(arrayDoneComment, 0, function () {
            window.Bunny.saveCommentSection(arrayComment, 0, function () {
                if (formSelector !== "") {
                    $(formSelector).submit();
                } else {
                    if (isNotReload == true) {

                    } else {
                        if (url !== "") {
                            window.location.href = url;
                        } else {
                            window.location.reload();
                        }
                    }
                }
            });
        });
    },

    saveDoneCommentSection: function (arrayDoneComment, index, callback) {
        if (index >= arrayDoneComment.length) {
            if (typeof callback === "function") {
                callback();
            }
            return;
        }

        var item = arrayDoneComment[index];
        if (item.isDone === true) {
            $.ajax({
                url: "/api/CommentApi/done/" + item.idComment,
                type: "get",
                DataType: "json",
                ContentType: "application/json",
                complete: function () {
                    index++;
                    window.Bunny.saveDoneCommentSection(arrayDoneComment, index, callback);
                }
            });
        } else {
            $.ajax({
                url: "/api/CommentApi/undone/" + item.idComment,
                type: "get",
                DataType: "json",
                ContentType: "application/json",
                complete: function () {
                    index++;
                    window.Bunny.saveDoneCommentSection(arrayDoneComment, index, callback);
                }
            });
        }
    },

    saveCommentSection: function (arrayComment, index, callback) {
        if (index >= arrayComment.length) {
            if (typeof callback === "function") {
                callback();
            }
            return;
        }

        var item = arrayComment[index];
        $.ajax({
            url: '/api/CommentApi/create',
            data: JSON.stringify(item),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //Bunny.notify("Add Success", "Add comment done");
            },
            error: function (err) {
                //Bunny.notify("Add Error", "Add comment fail, please try again", "error");
            },
            complete: function () {
                index++;
                window.Bunny.saveCommentSection(arrayComment, index, callback);
            }
        });
    },

    checkArrayCommentSection: function (array, isDelete) {
        var result = [];

        $.each(array, function (idx, comment) {
            var isNotDelete = true;
            $.each(isDelete, function (id, value) {
                if (idx === Number(value)) {
                    isNotDelete = false;
                    return;
                }
            });

            if (isNotDelete) {
                result.push(comment);
            }
        });

        //return array;
        return result;
    },

    numberERCustomizeTQ: function () {
        var i = 1;
        $(".content-ER-customizeTQ .form-group").each(function (index, ele) {
            if (!$(ele).hasClass("dis-none")) {
                var textTQ = $(ele).find("label.form-control-label").text().trim();
                var titleTQ = textTQ.split(". ");
                var textLabel = "";

                if ($.isNumeric(titleTQ[0]) === true) {
                    var numTQ = titleTQ[0] + ".";
                    textLabel = textTQ.replace(numTQ, "").trim();
                } else {
                    textLabel = textTQ;
                }

                $(ele).find("label.form-control-label").text(i + ". " + textLabel);
                i = i + 1;
            }
        });
    },
    initChangeMenuTQ: function () {
        $(".box-status-section").click(function () {
            if (window["isFromWorkRecord"]) {
                $(this).parent().find(".nav-link").trigger("click");
            }
        });
    },
    handleInputForReadonly: function (containerId, isNotDisable) {
        var except = ['Id', 'RedirecLink'];
        if (isNotDisable === 'False') {
            $(containerId + " :input").each(function (idx, item) {
                var type = $(item).attr('type');
                var id = $(item).attr('id');
                if (type !== 'button' && type !== 'hidden' && except.indexOf(id) === -1) {
                    var plugin = $(item).attr('data-plugin');

                    if (plugin === 'iCheck') {
                        $(item).iCheck('disable');
                    } else {
                        $(item).prop('disabled', true);
                    }
                }
            });

            //$(containerId + " :select").each(function (id, item) {
            //    $(item).prop('disabled', true);
            //});
        }  
    },
    validateNRIC: function (str) {
        if (str.length != 9)
            return false;
        str = str.toUpperCase();

        var i,
            icArray = [];
        for (i = 0; i < 9; i++) {
            icArray[i] = str.charAt(i);
        }

        icArray[1] = parseInt(icArray[1], 10) * 2;
        icArray[2] = parseInt(icArray[2], 10) * 7;
        icArray[3] = parseInt(icArray[3], 10) * 6;
        icArray[4] = parseInt(icArray[4], 10) * 5;
        icArray[5] = parseInt(icArray[5], 10) * 4;
        icArray[6] = parseInt(icArray[6], 10) * 3;
        icArray[7] = parseInt(icArray[7], 10) * 2;

        var weight = 0;
        for (i = 1; i < 8; i++) {
            weight += icArray[i];
        }

        var offset = (icArray[0] == "T" || icArray[0] == "G") ? 4 : 0;
        var temp = (offset + weight) % 11;

        var st = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
        var fg = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];

        var theAlpha;
        if (icArray[0] == "S" || icArray[0] == "T") {
            theAlpha = st[temp];
        } else if (icArray[0] == "F" || icArray[0] == "G") {
            theAlpha = fg[temp];
        }

        return (icArray[8] === theAlpha);
    }
};

$(function () {
    window.Bunny.setupAjax();
    window.Bunny.initChangeMenuTQ();
    window.Bunny.initToolTip();
    window.Bunny.initConfirmDialog();
    window.Bunny.inBunnylidePanel();
    window.Bunny.initDatePicker();
    window.Bunny.initAutoRenderSlug();
    window.Bunny.initDropify();
    window.Bunny.initialMenubar();
    window.Bunny.initCountryDropdown();
    window.Bunny.initNationalDropdown();
    window.Bunny.initAutoDecimalFormat();
    window.Bunny.initChangeValidationModal();
    window.Bunny.initDefaultValidationModal();
    window.Bunny.inBunnyummernote();
    window.Bunny.initLadda();
    window.Bunny.setCookieSubmit("IsAutoSubmitTQ", false, -1);
    window.Bunny.setCookieSubmit("IsAutoInternalSubmitTQ", false, -1);
    window.Bunny.scrollFocusWithId();
    window.Bunny.keyPressHandler();
});

String.prototype.preventInjection = function preventInjection() {
    return this.replace(/</g, "&lt;").replace(/>/g, "&gt;");
};

String.prototype.genSlug = function changeToSlug() {
    var title, slug;
    title = this;

    slug = title.toLowerCase();

    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, "a");
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, "e");
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, "i");
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, "o");
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, "u");
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, "y");
    slug = slug.replace(/đ/gi, "d");

    slug = slug.replace(/c#/gi, "c-sharp");

    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, "");

    slug = slug.replace(/ /gi, "-");

    slug = slug.replace(/\-\-\-\-\-/gi, "-");
    slug = slug.replace(/\-\-\-\-/gi, "-");
    slug = slug.replace(/\-\-\-/gi, "-");
    slug = slug.replace(/\-\-/gi, "-");

    slug = "@" + slug + "@";
    slug = slug.replace(/\@\-|\-\@|\@/gi, "");

    return slug;
};

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

Array.prototype.contains = function (element) {
    return this.indexOf(element) > -1;
};
