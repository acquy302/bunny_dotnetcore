﻿using System.Threading.Tasks;
using Bunny.Middle.Auth.Filters;
using Bunny.Middle.User;
using Bunny.Model.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Controllers.Components
{
    [ServiceFilter(typeof(LoggedInUserBinderFilter))]
    public class HeaderViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(bool isAdminPortal)
        {
            var loggedInUserModel = LoggedInUser.Current;
            var result = new UserHeaderViewModel
            {
                FirstName = loggedInUserModel?.FirstName,
                LastName = loggedInUserModel?.LastName,
                Id = loggedInUserModel?.Id ?? 0,
                IsAdminPortal = isAdminPortal,
            };
            return await Task.FromResult<IViewComponentResult>(View("Header", result)).ConfigureAwait(false);
        }
    }
}