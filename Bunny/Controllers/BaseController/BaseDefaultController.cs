﻿using Bunny.Middle.Auth.Attributes;
using Bunny.Middle.Auth.Filters;
using Bunny.Swagger.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Controllers.BaseController
{
    [HideInDocs]
    [Auth]
    [ServiceFilter(typeof(LoggedInUserBinderFilter))]
    [ServiceFilter(typeof(MvcAuthActionFilter))]
    //[ServiceFilter(typeof(ActionFilter))]
    public class BaseDefaultController : Controller
    {
    }
}