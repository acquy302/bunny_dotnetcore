﻿using System.Threading.Tasks;
using Bunny.Areas.Web.Controllers.BaseController;
using Bunny.DataTable;
using Bunny.DataTable.Models.Request;
using Bunny.Extensions;
using Bunny.Model.User;
using Bunny.Service.Interface.User;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Web.Controllers
{
    [Route(Endpoint)]
    public class UserController : BaseWebController
    {
        public const string Endpoint = AreaName + "/user";
        public const string UserEndpoint = "index";
        public const string ListingEndpoint = "";
        public const string EditEndpoint = "edit";
        public const string SubmitEditEndpoint = "submit-edit";
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // GET
        [Route(UserEndpoint)]
        [HttpGet]
        public IActionResult Index()
        {
            return
            View();
        }
        /// <summary>
        ///     Edit 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route(EditEndpoint)]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _userService.GetEdit(id).ConfigureAwait(true);
            return View(model);
        }
        [Route(SubmitEditEndpoint)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> SubmitEdit([FromBody]UserEditModel model)
        {
            if (!ModelState.IsValid)
            {
                //this.SetNotify("Edit Fail", "Edit user fail, please try again", NotifyStatus.Error);
                return Json("Fail");
            }
            this.SetNotify("Edit Success", "User has been updated successful", NotifyStatus.Error);
            return Json("Success");
        }
    }
}