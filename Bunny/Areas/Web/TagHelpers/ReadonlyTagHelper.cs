﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Bunny.Areas.Web.TagHelpers
{
    [HtmlTargetElement("input")]
    [HtmlTargetElement("a")]
    [HtmlTargetElement("textarea")]
    [HtmlTargetElement("select")]
    [HtmlTargetElement("button")]
    [HtmlTargetElement("div")]
    [HtmlTargetElement("span")]
    [HtmlTargetElement("label")]
    [HtmlTargetElement("li")]
    [HtmlTargetElement("ul")]
    [HtmlTargetElement("nav")]
    [HtmlTargetElement("script")]
    [HtmlTargetElement("style")]
    public class ReadonlyTagHelper : TagHelper
    {
        private const string ReadonlyAttributeName = "asp-readonly";

        [HtmlAttributeName(ReadonlyAttributeName)]
        public bool IsReadonly { set; get; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (IsReadonly)
            {
                var attribute = new TagHelperAttribute("readonly", "readonly");

                output.Attributes.Add(attribute);
            }

            base.Process(context, output);
        }
    }
}