﻿using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Bunny.Areas.Api.Controllers.BaseController;
using Bunny.Service.Interface.Test;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class TestApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/system";
        public const string TestCreateUserEndpoint = "test-create-user";
        public const string GetAllUserEndpoint = "get-all-user";

        private readonly ITestService _testService;

        public TestApiController(ITestService testService)
        {
            _testService = testService;
        }
        /// <summary>
        ///     Using for test create user entity
        /// </summary>
        /// <returns></returns>
        [Route(TestCreateUserEndpoint)]
        [HttpGet]
        public async Task<IActionResult> TestCreateUser()
        {
            var result = await _testService.TestAddEntity().ConfigureAwait(false);
            return Json(result);
        }

        /// <summary>
        ///     Get all user entity
        /// </summary>
        /// <returns></returns>
        [Route(GetAllUserEndpoint)]
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            var result = await _testService.GetAllUserEntity().ConfigureAwait(false);
            return Json(result);
        }
    }
}