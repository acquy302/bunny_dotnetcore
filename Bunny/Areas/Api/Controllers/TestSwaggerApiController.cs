﻿
using System.Threading.Tasks;
using Bunny.Areas.Api.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Areas.Api.Controllers
{
    [Route(EndPoint)]
    public class TestSwaggerApiController : BaseApiController
    {
        public const string EndPoint = AreaName + "/system";
        public const string TestSwaggerEndpoint = "test-swagger";
        // GET
        [Route(TestSwaggerEndpoint)]
        [HttpGet]
        public async Task<IActionResult> TestSwagger()
        {
            //var result = await _testService.TestAddEntity().ConfigureAwait(false);
            return Ok("OK");
        }
    }
}