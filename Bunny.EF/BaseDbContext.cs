﻿using Bunny.EF.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Bunny.EF
{
    public abstract class BaseDbContext : DbContext, IBaseDbContext
    {
        protected BaseDbContext()
        {
        }

        protected BaseDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}