﻿using System;
using Bunny.EF.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.EF
{
    public static class MigrationExtensions
    {
        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        public static IServiceProvider MigrateDatabase<T>(this IServiceProvider services) where T : IBaseDbContext
        {
            T dbContext = services.GetRequiredService<T>();
            dbContext.Database.Migrate();
            return services;
        }

        /// <summary>
        ///     <para>
        ///         Applies any pending migrations for the context to the database. Will create the
        ///         database if it does not already exist.
        ///     </para>
        /// </summary>
        public static IApplicationBuilder MigrateDatabase<T>(this IApplicationBuilder app) where T : IBaseDbContext
        {
            T dbContext = app.ApplicationServices.GetService<T>();
            dbContext.Database.Migrate();
            return app;
        }
    }
}