﻿using System.Collections.Generic;
using System.Linq;
using Bunny.EF.Interfaces.Entities;

namespace Bunny.EF.Extensions
{
    public static class SoftDeletableEntityExtensions
    {
        /// <summary>
        ///     Filter entities is not deleted by <c> DeletedTime != null </c> 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereNotDeleted<T>(this IQueryable<T> query) where T : class, ISoftDeletableEntity
        {
            query = query.Where(x => x.DeletedTime == null);
            return query;
        }

        /// <summary>
        ///     Filter entities is not deleted by <c> DeletedTime != null </c> 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="iEnumerable"></param>
        /// <returns></returns>
        public static IEnumerable<T> WhereNotDeleted<T>(this IEnumerable<T> iEnumerable) where T : class, ISoftDeletableEntity
        {
            iEnumerable = iEnumerable.AsQueryable().WhereNotDeleted();
            return iEnumerable;
        }
    }
}