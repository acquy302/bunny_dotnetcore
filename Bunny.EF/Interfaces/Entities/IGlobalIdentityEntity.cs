﻿namespace Bunny.EF.Interfaces.Entities
{
    public interface IGlobalIdentityEntity
    {
        string GlobalId { get; set; }
    }
}