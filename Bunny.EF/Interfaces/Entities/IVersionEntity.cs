﻿using System.ComponentModel.DataAnnotations;

namespace Bunny.EF.Interfaces.Entities
{
    public interface IVersionEntity
    {
        [Timestamp]
        byte[] Version { get; set; }
    }
}