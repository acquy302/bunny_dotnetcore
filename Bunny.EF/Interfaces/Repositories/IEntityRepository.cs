﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Bunny.EF.Entities;

namespace Bunny.EF.Repositories
{
    public interface IEntityRepository<TEntity, TKey> : IEntityBaseRepository<TEntity> where TEntity : Entity<TKey>, new() where TKey : struct
    {
        void UpdateWhere(Expression<Func<TEntity, bool>> predicate, TEntity entityNewData, params Expression<Func<TEntity, object>>[] changedProperties);

        void UpdateWhere(Expression<Func<TEntity, bool>> predicate, TEntity entityNewData, params string[] changedProperties);

        void DeleteWhere(List<TKey> listId, bool isPhysicalDelete = false);
    }
}