﻿using Bunny.EF.Interfaces.Entities;

namespace Bunny.EF.Entities
{
    /// <inheritdoc cref="EntityBase" />
    /// <summary>
    ///     Entity for Entity Framework
    /// </summary>
    /// <typeparam name="TKey">Id type of this entity</typeparam>
    public abstract class Entity<TKey> : EntityBase, ISoftDeletableEntity<TKey>, IAuditableEntity<TKey> where TKey : struct
    {
        public TKey Id { get; set; }

        public TKey? CreatedBy { get; set; }

        public TKey? LastUpdatedBy { get; set; }

        public TKey? DeletedBy { get; set; }
    }
    public abstract class Entity : Entity<int>
    {
    }
}