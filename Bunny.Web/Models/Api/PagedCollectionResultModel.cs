﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Bunny.Web.Models.Api
{
    [Serializable]
    [KnownType(typeof(PagedCollectionResultModel<>))]
    public class PagedCollectionResultModel<T>
    {
        public int Skip { get; set; }

        public int Take { get; set; }

        public string Terms { get; set; }

        public long Total { get; set; }

        public List<T> Items { get; set; } = new List<T>();

        [JsonExtensionData]
        public Dictionary<string, object> AdditionalData { get; set; } = new Dictionary<string, object>();
    }
}