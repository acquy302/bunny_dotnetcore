﻿using Bunny.Core.StringUtils;
using Microsoft.AspNetCore.Routing;

namespace Bunny.Web.RouteUtils
{
    public static class RouteValueDictionaryExtensions
    {
        public static string GetUrlWithQueries(this RouteValueDictionary routeValueDictionary, string url)
        {
            foreach (var newLinkValue in routeValueDictionary)
            {
                var hrefKey = "{" + newLinkValue.Key + "}";
                if (url.Contains(hrefKey))
                {
                    url = url.Replace(hrefKey, newLinkValue.Value?.ToString() ?? string.Empty);
                }
                else
                {
                    var hrefValue = newLinkValue.Value?.ToString();

                    if (string.IsNullOrWhiteSpace(hrefValue))
                        continue;

                    var query = $"{newLinkValue.Key}={hrefValue}";
                    url = url.AddQueryString(query);
                }
            }

            return url;
        }

        public static void SafeSetValue(this RouteValueDictionary routeValueDictionary, string key, object value)
        {
            if (routeValueDictionary.ContainsKey(key))
                routeValueDictionary.Remove(key);
            routeValueDictionary.Add(key, value);
        }
    }
}