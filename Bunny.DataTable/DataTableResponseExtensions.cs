﻿using System;
using System.Linq;
using Bunny.DataTable.Models.Request;
using Bunny.DataTable.Models.Response;
using Bunny.DataTable.Utils;
using Bunny.DataTable.Utils.Reflection;

namespace Bunny.DataTable
{
    public static class DataTableResponseExtensions
    {
        public static DataTableResponseDataModel<T> GetDataTableResponse<T>(this IQueryable<T> data, DataTableParamModel dataTableParamModel) where T : class, new()
        {
            var totalRecords = data.Count(); // annoying this, as it causes an extra evaluation..

            var filters = new DataTableFiltering();

            var outputProperties = DataTableTypeInfo<T>.Properties;

            var filteredData = filters.ApplyFiltersAndSort(dataTableParamModel, data, outputProperties);

            var totalDisplayRecords = filteredData.Count();

            var skipped = filteredData.Skip(dataTableParamModel.DisplayStart);

            var page = (dataTableParamModel.DisplayLength <= 0 ? skipped : skipped.Take(dataTableParamModel.DisplayLength)).ToArray();

            var result = new DataTableResponseDataModel<T>
            {
                TotalRecord = totalRecords,
                TotalDisplayRecord = totalDisplayRecords,
                Echo = dataTableParamModel.Echo,
                Data = page.Cast<object>().ToArray()
            };

            return result;
        }

        public static DataTableActionResult<T> GetDataTableActionResult<T>(this DataTableResponseDataModel<T> responseData, Func<T, object> transform, ResponseOptionModel<T> responseOption = null) where T : class, new()
        {
            return DataTableActionResult.Create(responseData, transform, responseOption);
        }

        public static DataTableActionResult<T> GetDataTableActionResult<T>(this DataTableResponseDataModel<T> responseData, ResponseOptionModel<T> responseOption = null) where T : class, new()
        {
            return DataTableActionResult.Create(responseData, responseOption);
        }
    }
}