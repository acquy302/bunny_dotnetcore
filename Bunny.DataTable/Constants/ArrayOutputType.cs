﻿namespace Bunny.DataTable.Constants
{
    public enum ArrayOutputType
    {
        BiDimensionalArray,
        ArrayOfObjects
    }
}