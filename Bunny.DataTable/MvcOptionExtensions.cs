﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.DataTable
{
    public static class MvcOptionExtensions
    {
        public static void AddDataTableModelBinderProvider(this MvcOptions opts)
        {
            var isProviderAdded = opts.ModelBinderProviders.Any(x => x.GetType() == typeof(DataTablesModelBinderProvider));

            if (isProviderAdded) return;

            opts.ModelBinderProviders.Insert(0, new DataTablesModelBinderProvider());
        }
    }
}