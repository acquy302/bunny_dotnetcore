﻿using System;
using System.Reflection;
using Bunny.DataTable.Models.Config.Column;

namespace Bunny.DataTable.Attributes
{
    public abstract class DataTableAttributeBase : Attribute
    {
        public abstract void ApplyTo(ColumnModel columnModel, PropertyInfo propertyInfo);
    }
}