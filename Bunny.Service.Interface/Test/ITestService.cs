﻿using System.Collections.Generic;
using Bunny.Data.EF.Entities;
using System.Threading;
using System.Threading.Tasks;
using Bunny.Model.User;

namespace Bunny.Service.Interface.Test
{
    public interface ITestService
    {
        Task<UserEntity> TestAddEntity(CancellationToken cancellationToken = default);
        Task<List<UserViewModel>> GetAllUserEntity(CancellationToken cancellationToken = default);
    }
}