﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bunny.Middle
{
    public class Enums
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public enum BooleanNA
        {
            [Display(Name = "Unsure")]
            NA = 0,

            [Display(Name = "Yes")]
            Yes = 1,

            [Display(Name = "No")]
            No = 2,
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum BooleanNotNA
        {
            [Display(Name = "Yes")]
            Yes = 0,

            [Display(Name = "No")]
            No = 1,
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum Nationality
        {
            //[Display(Name = "NA")]
            //NA = 0,

            [Display(Name = "Foreigner")]
            Foreigner = 1,

            [Display(Name = "Singapore Citizen")]
            SingaporeCitizen = 2
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum Role
        {
            [Display(Name = "Administrator")]
            Admin = 1,

            [Display(Name = "Manager")]
            Manager = 2,

            [Display(Name = "Staff")]
            Staff = 3,

            [Display(Name = "Normal")]
            Normal = 4,
        }
    }
}