﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Bunny.Middle.Auth.Filters
{
    public class ApiAuthActionFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.IsAuthenticated())
            {
                context.Result = new StatusCodeResult(StatusCodes.Status410Gone);
                return;
            }

            if (!context.IsAuthorized())
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
            }
            //
            var isAllowAnonymous = context.Filters.Any(item => item is IAllowAnonymousFilter);
            if (!context.HttpContext.User.Identity.IsAuthenticated && !isAllowAnonymous)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
            }
        }
    }
}