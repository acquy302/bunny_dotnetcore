﻿using System.Threading;
using System.Threading.Tasks;
using Bunny.Middle.Model.User;
using Bunny.Model.MasterPage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Pages.Account.Manage.Internal;
using Microsoft.AspNetCore.Mvc;

namespace Bunny.Middle.Auth.Interfaces
{
    public interface IAuthenticationService
    {
        /// <summary>
        ///     Get access token and get <see cref="LoggedInUserModel" /> data for
        ///     LoggedInUser.Current, ClaimsPrincipal for HttpContext.User
        /// </summary>
        /// <param name="httpContext">      </param>
        /// <param name="model">            </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<AccessTokenModel> SignInAsync(HttpContext httpContext, RequestTokenModel model, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Set Cookie in Response and Get <see cref="LoggedInUserModel" /> data for
        ///     LoggedInUser.Current, ClaimsPrincipal for HttpContext.User
        /// </summary>
        /// <param name="httpContext">      </param>
        /// <param name="accessTokenModel"> </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task SignInCookieAsync(HttpContext httpContext, AccessTokenModel accessTokenModel, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Get valid (not check expire) access token and get <see cref="LoggedInUserModel" />
        ///     data for LoggedInUser.Current, ClaimsPrincipal for HttpContext.User
        /// </summary>
        /// <param name="httpContext">      </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<AccessTokenModel> SignInCookieAsync(HttpContext httpContext, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Remove Cookie value and Set null for LoggedInUser.Current, null for HttpContext.User 
        /// </summary>
        /// <param name="httpContext">      </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task SignOutCookieAsync(HttpContext httpContext, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Get <see cref="LoggedInUserModel" /> from valid access token 
        /// </summary>
        /// <param name="accessToken">      </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<LoggedInUserModel> GetLoggedInUserAsync(string accessToken, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Expire all refresh token by valid access token, this method make user need Sign In again
        /// </summary>
        /// <param name="accessToken">      </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ExpireAllRefreshTokenAsync(string accessToken, CancellationToken cancellationToken = default);

        Task SendConfirmEmailOrSetPasswordAsync(string email, CancellationToken cancellationToken = default);

        Task ConfirmEmailAsync(SetPasswordModel model, CancellationToken cancellationToken = default);

        bool IsExpireOrInvalidConfirmEmailToken(string token);

        Task SetPasswordAsync(SetPasswordModel model, CancellationToken cancellationToken = default);

        bool IsExpireOrInvalidSetPasswordToken(string token);
        bool IsUserAccess(int id);

        /// <summary>
        ///     Check current password of logged in user 
        /// </summary>
        /// <param name="currentPassword"></param>
        void CheckCurrentPassword(string currentPassword);

        Task ChangePasswordAsync(ChangePasswordModel model, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Get url background in configuration 
        /// </summary>
        /// <param name="cancellationToken"></param>
        Task<string> GetUrlBackgroundAsync(CancellationToken cancellationToken = default);
        //EditSMTPConfigurationModel GetLoginConfiguration(CancellationToken cancellationToken = default);
        Task SendEmailDelegatedAsync(IUrlHelper url, string token, CancellationToken cancellationToken = default);

    }
}