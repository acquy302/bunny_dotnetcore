﻿using System;
using System.Linq;
using Bunny.Core.ServiceCollectionUtils;
using Bunny.Middle.Auth.Filters;
using Bunny.Web.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;

namespace Bunny.Middle.Auth
{
    public static class ServiceCollectionExtensions
    {
        private static IConfiguration _configuration;
        private static string _configSection;

        /// <summary>
        ///     [Authentication] Json Web Token + Cookie 
        /// </summary>
        /// <param name="services">     </param>
        /// <param name="configuration"></param>
        /// <param name="configSection"></param>
        /// <returns></returns>
        public static IServiceCollection AddHybridAuth(this IServiceCollection services, IConfiguration configuration, string configSection = Constants.Constant.DefaultConfigSection)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _configSection = configSection;
            configuration.BuildConfig(configSection);

            services.AddScopedIfNotExist<ApiAuthActionFilter>();
            services.AddScopedIfNotExist<MvcAuthActionFilter>();
            services.AddScopedIfNotExist<LoggedInUserBinderFilter>();
            services.AddScopedIfNotExist<ActionFilter>();

            // Add System.HttpContext.Current
            services.AddHttpContextAccessor();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = AuthConfig.TokenValidationParameters;
                });

            return services;
        }

        /// <summary>
        ///     [Authentication] Json Web Token + Cookie 
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        /// <remarks>
        ///     The global config for HybridAuth from appsettings.json will auto reload when you
        ///     change config. Be careful when you change the SecretKey, it affect to existing
        ///     PasswordHash and Token
        /// </remarks>
        public static IApplicationBuilder UseHybridAuth(this IApplicationBuilder app)
        {
            _configuration.BuildConfig(_configSection);

            // Use System.HttpContext.Current
            app.UseHttpContextAccessor();

            ChangeToken.OnChange(_configuration.GetReloadToken, () =>
            {
                // Re-Build the config
                _configuration.BuildConfig(_configSection);
            });

            app.UseAuthentication();

            return app;
        }

        public static void BuildConfig(this IConfiguration configuration, string configSection = Constants.Constant.DefaultConfigSection)
        {
            var isHaveConfig = configuration.GetChildren().Any(x => x.Key == configSection);

            if (isHaveConfig)
            {
                AuthConfig.SecretKey = configuration.GetValue($"{configSection}:{nameof(AuthConfig.SecretKey)}", AuthConfig.SecretKey);
                AuthConfig.AccessTokenExpireIn = configuration.GetValue($"{configSection}:{nameof(AuthConfig.AccessTokenExpireIn)}", AuthConfig.AccessTokenExpireIn);
            }
        }
    }
}