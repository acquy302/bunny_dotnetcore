﻿using System.Collections.Generic;
using System.Linq;
using Bunny.Core.Constants;
using Bunny.Core.TypeUtils;
using System.Web;
using Bunny.Core.DictionaryUtils;
using Bunny.Core.ObjectUtils;
using Bunny.Middle.Model.User;

namespace Bunny.Middle.User
{
    public static class LoggedInUser
    {
        private static string HttpContextItemKey => typeof(LoggedInUser).GetAssembly().FullName;

        public static LoggedInUserModel Current
        {
            get
            {
                if (HttpContext.Current?.Items != null)
                {
                    return HttpContext.Current.Items.TryGetValue(HttpContextItemKey, out var value)
                        ? value?.ConvertTo<LoggedInUserModel>()
                        : null;
                }

                return null;
            }
            set
            {
                // Update Current Logged In User in both Static Global variable and HttpContext
                //if (HttpContext.Current.Items?.Any() != null)
                //{
                //    HttpContext.Current.Items = new Dictionary<object, object>();
                //}

                if (value == null)
                {
                    if (HttpContext.Current.Items?.ContainsKey(HttpContextItemKey) == true)
                    {
                        HttpContext.Current.Items.Remove(HttpContextItemKey);
                    }
                    return;
                }

                value.AccessToken = HttpContext.Current.Request.Headers[HeaderKey.Authorization].ToString()?.Split(' ')
                    .LastOrDefault();

                value.AccessTokenType = HttpContext.Current.Request.Headers[HeaderKey.Authorization].ToString()?.Split(' ').FirstOrDefault();

                HttpContext.Current.Items.AddOrUpdate(HttpContextItemKey, value);
            }
        }
    }
}