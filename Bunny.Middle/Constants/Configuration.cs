﻿namespace Bunny.Middle.Constants
{
    public static class Configuration
    {
        // Config File Name
        public const string AppSettingsJsonFileName = "appsettings.json";

        public const string BundleConfigJsonFileName = "bundleconfig.json";
        public const string CompilerConfigJsonFileName = "compilerconfig.json";

        // Config Section
        public const string ConnectionStringsConfigSection = "ConnectionStrings";
    }
}