﻿namespace Bunny.Middle.Configs.Models
{
    public class SendGridConfigModel
    {
        public string Key { get; set; }

        public string SenderDisplayEmail { get; set; }

        public string SenderDisplayName { get; set; }
    }
}