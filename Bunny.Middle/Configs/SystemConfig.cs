﻿using System;
using Bunny.Middle.Configs.Models;

namespace Bunny.Middle.Configs
{
    public static class SystemConfig
    {
        public static DateTimeOffset SystemVersion { get; set; }

        /// <summary>
        ///     Production, Staging will read from key Environment Name, else by MachineName 
        /// </summary>
        public static string DatabaseConnectionString { get; set; }

        public static string CRMDatabaseConnectionString { get; set; }
        public static string CountryDataConfig { get; set; }
        public static string NationalityDataConfig { get; set; }

        /// <summary>
        ///     Production, Staging will read from key Environment Name, else by MachineName 
        /// </summary>
        public static string LogDatabaseConnectionString { get; set; }

        public static bool IsUseLogDatabase { get; set; }

        public static bool IsEnableSendMailInternal { get; set; }
        public static bool IsEnableFinalCode { get; set; }
        public static string ExtendMailInternal { get; set; }
        public static string PrefixRedis { get; set; }
        public static TimeSpan ExternalDataSyncAtTime { get; set; } = TimeSpan.FromHours(9);

        //Exchange rate
        public static string ApiExchangeRate { get; set; }
        public static string LevelGetResult { get; set; }
        public static string FieldsExchangeRate { get; set; }
        public static string FormatQueryExchangeRate { get; set; }
        public static string DisplayFieldsExchangeRate { get; set; }

        public static TimeSpan ExternalDataSyncTimeSpan { get; set; } = TimeSpan.FromDays(1);

        /// <summary>
        ///     Config use datetime with TimeZone. Default is "UTC", See more: https://msdn.microsoft.com/en-us/library/gg154758.aspx 
        /// </summary>
        public static string SystemTimeZone { get; set; } = "UTC";

        public static string SystemDateFormat { get; set; } = "dd/MM/yyyy";

        public static string SystemDateTimeFormat { get; set; } = "dd/MM/yyyy HH:mm:ss";

        /// <summary>
        ///     Folder Name of wwwroot, Areas and Areas name and request path Config 
        /// </summary>
        public static MvcPathConfigModel MvcPath { get; set; } = new MvcPathConfigModel();

        /// <summary>
        ///     [Auto Reload] 
        /// </summary>
        public static PagedCollectionParametersConfigModel PagedCollectionParameters { get; set; } = new PagedCollectionParametersConfigModel();

        /// <summary>
        ///     [Auto Reload] 
        /// </summary>
        public static SendGridConfigModel SendGrid { get; set; } = new SendGridConfigModel();

        public static string SystemDomainUrl { get; set; }

        public static string KickoffApiAccessKey { get; set; }
    }
}