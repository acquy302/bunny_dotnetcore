﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Bunny.Middle.Configs;
using Bunny.Core.ConfigUtils;
using Bunny.Core.HttpUtils;
using Bunny.Core.StringUtils;
using Bunny.Middle.Configs.Models;

namespace Bunny.Middle.Configs
{
    public static class SystemConfigServiceExtensions
    {
        public static IServiceCollection AddSystemConfiguration(this IServiceCollection services, IHostingEnvironment hostingEnvironment, IConfigurationRoot configurationRoot)
        {
            // Add Service
            services.AddSingleton(hostingEnvironment);
            services.AddSingleton(configurationRoot);
            services.AddSingleton<IConfiguration>(configurationRoot);

            // Build System Config
            SystemConfigurationHelper.BuildSystemConfig(configurationRoot);

            return services;
        }

        public static IApplicationBuilder UseSystemConfiguration(this IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            // Currently, ASPNETCORE have an issue hit twice when change appsetting.json from
            // 20/03/17 (see more: https://github.com/aspnet/SystemConfigs/issues/624) And please
            // don't use IOption and IOptionSnapshot, it harder to manage lifetime of object and mad
            // of Injection I assumption configuration is singleton and use Statics class to do it.
            // Keep Simple everything Possible.

            IConfigurationRoot configurationRoot = app.ApplicationServices.GetService<IConfigurationRoot>();

            ChangeToken.OnChange(configurationRoot.GetReloadToken, () =>
            {
                // Build System Config
                SystemConfigurationHelper.BuildSystemConfig(configurationRoot);
            });

            app.UseMiddleware<SystemDomainMiddleware>();

            return app;
        }
        public static class SystemConfigurationHelper
        {
            public static void BuildSystemConfig(IConfiguration configuration)
            {
                // Connection String
                SystemConfig.DatabaseConnectionString = configuration.GetValueByMachineAndEnv<string>("ConnectionStrings");
                SystemConfig.CRMDatabaseConnectionString = configuration.GetValueByMachineAndEnv<string>("CRMConnectionStrings");
                SystemConfig.LogDatabaseConnectionString = configuration.GetValueByMachineAndEnv<string>("LogConnectionStrings");
                SystemConfig.IsUseLogDatabase = configuration.GetValueByMachineAndEnv<bool>(nameof(SystemConfig.IsUseLogDatabase));

                SystemConfig.MvcPath = configuration.GetSection<MvcPathConfigModel>(nameof(SystemConfig.MvcPath)) ?? new MvcPathConfigModel();

                //country
                SystemConfig.CountryDataConfig = configuration.GetValue(nameof(SystemConfig.CountryDataConfig), SystemConfig.CountryDataConfig);
                SystemConfig.NationalityDataConfig = configuration.GetValue(nameof(SystemConfig.NationalityDataConfig), SystemConfig.NationalityDataConfig);


                // Time Zone
                SystemConfig.SystemTimeZone = configuration.GetValue(nameof(SystemConfig.SystemTimeZone), SystemConfig.SystemTimeZone);
                SystemUtils.SystemTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(SystemConfig.SystemTimeZone);

                // Date and Date Time Format
                SystemConfig.SystemDateFormat = configuration.GetValue(nameof(SystemConfig.SystemDateFormat), SystemConfig.SystemDateFormat);
                SystemConfig.SystemDateTimeFormat = configuration.GetValue(nameof(SystemConfig.SystemDateTimeFormat), SystemConfig.SystemDateTimeFormat);

                // Others
                SystemConfig.MvcPath = configuration.GetSection<MvcPathConfigModel>(nameof(SystemConfig.MvcPath)) ?? new MvcPathConfigModel();
                SystemConfig.PagedCollectionParameters = configuration.GetSection<PagedCollectionParametersConfigModel>(nameof(SystemConfig.PagedCollectionParameters)) ?? new PagedCollectionParametersConfigModel();
                SystemConfig.SendGrid = configuration.GetSection<SendGridConfigModel>(nameof(SystemConfig.SendGrid)) ?? new SendGridConfigModel();

                // Access Key
                SystemConfig.KickoffApiAccessKey = configuration.GetValue<string>(nameof(SystemConfig.KickoffApiAccessKey));

                // Schedule
                SystemConfig.ExternalDataSyncAtTime = configuration.GetValue(nameof(SystemConfig.ExternalDataSyncAtTime), SystemConfig.ExternalDataSyncAtTime);
                SystemConfig.ExternalDataSyncTimeSpan = configuration.GetValue(nameof(SystemConfig.ExternalDataSyncTimeSpan), SystemConfig.ExternalDataSyncTimeSpan);

                //Config send mail
                SystemConfig.IsEnableSendMailInternal = configuration.GetValue(nameof(SystemConfig.IsEnableSendMailInternal), SystemConfig.IsEnableSendMailInternal);
                SystemConfig.PrefixRedis = configuration.GetValue(nameof(SystemConfig.PrefixRedis), SystemConfig.PrefixRedis);
                SystemConfig.ExtendMailInternal = configuration.GetValue(nameof(SystemConfig.ExtendMailInternal), SystemConfig.ExtendMailInternal);

                //FinalCode
                SystemConfig.IsEnableFinalCode = configuration.GetValue(nameof(SystemConfig.IsEnableFinalCode), SystemConfig.IsEnableFinalCode);

                //Exchange Rate currency
                SystemConfig.ApiExchangeRate = configuration.GetValue(nameof(SystemConfig.ApiExchangeRate), SystemConfig.ApiExchangeRate);
                SystemConfig.LevelGetResult = configuration.GetValue(nameof(SystemConfig.LevelGetResult), SystemConfig.LevelGetResult);
                SystemConfig.FieldsExchangeRate = configuration.GetValue(nameof(SystemConfig.FieldsExchangeRate), SystemConfig.FieldsExchangeRate);
                SystemConfig.DisplayFieldsExchangeRate = configuration.GetValue(nameof(SystemConfig.DisplayFieldsExchangeRate), SystemConfig.DisplayFieldsExchangeRate);
                SystemConfig.FormatQueryExchangeRate = configuration.GetValue(nameof(SystemConfig.FormatQueryExchangeRate), SystemConfig.FormatQueryExchangeRate);

            }
        }

        public class SystemDomainMiddleware
        {
            private readonly RequestDelegate _next;

            public SystemDomainMiddleware(RequestDelegate next)
            {
                _next = next;
            }

            public Task Invoke(HttpContext context)
            {
                SystemConfig.SystemDomainUrl = context.Request.GetDomain();

                // Make sure System Domain Url not end by /
                SystemConfig.SystemDomainUrl = SystemConfig.SystemDomainUrl?.CleanUrlPath();

                return _next(context);
            }
        }
    }
}