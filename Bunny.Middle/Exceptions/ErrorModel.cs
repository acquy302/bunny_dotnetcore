﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EnumsNET;
using Newtonsoft.Json;

namespace Bunny.Middle.Exceptions
{
    public class ErrorModel
    {
        public ErrorModel()
        {
        }

        public ErrorModel(ErrorCode code, string message)
        {
            Code = code;
            Message = string.IsNullOrWhiteSpace(message) ? code.AsString(EnumFormat.Description) : message;
            Module = EnumsNET.Enums.GetMember<ErrorCode>(code.ToString()).Value.GetAttributes().Get<DisplayAttribute>().GetGroupName();
        }

        public ErrorModel(ErrorCode code, string message, Dictionary<string, object> additionalData) : this(code, message)
        {
            AdditionalData = additionalData;
        }

        public string Id { get; set; }

        /// <summary>
        ///     Unique Code for each Business 
        /// </summary>
        public ErrorCode Code { get; set; } = ErrorCode.Unknown;

        /// <summary>
        ///     Message description for Client App Developer 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        ///     Module of business 
        /// </summary>
        public string Module { get; set; }

        [JsonExtensionData]
        public Dictionary<string, object> AdditionalData { get; set; } = new Dictionary<string, object>();
    }
}