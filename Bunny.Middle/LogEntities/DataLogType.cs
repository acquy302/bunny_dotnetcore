﻿namespace Bunny.Middle.LogEntities
{
    public enum DataLogType
    {
        Added = 1,
        Modified = 2,
        SoftDeleted = 3,
        PhysicalDeleted = 4
    }
}