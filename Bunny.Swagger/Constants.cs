﻿using System;
using Microsoft.AspNetCore.Http;

namespace Bunny.Swagger
{
    public static class Constants
    {
        public const string ApiDocAssetFolderPath = "apidoc";

        public static readonly string IndexHtmlPath = $"{ApiDocAssetFolderPath}/index.html";

        public static readonly string ViewerHtmlPath = $"{ApiDocAssetFolderPath}/json-viewer.html";

        public static readonly PathString ApiDocAssetRequestPath = new PathString("/.well-known/api-define/assets");

        public static readonly TimeSpan? ApiDocAssetMaxAgeResponseHeader = new TimeSpan(365, 0, 0, 0);

        public const string DefaultConfigSection = "ApiDocument";
    }
}