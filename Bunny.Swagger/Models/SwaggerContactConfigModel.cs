﻿namespace Bunny.Swagger.Models
{
    public class SwaggerContactConfigModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string Email { get; set; }
    }
}