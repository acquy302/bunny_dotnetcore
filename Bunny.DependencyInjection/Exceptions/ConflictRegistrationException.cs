﻿using System;

namespace Bunny.DependencyInjection.Exceptions
{
    public class ConflictRegistrationException : Exception
    {
        public ConflictRegistrationException(string message) : base(message)
        {
        }
    }
}