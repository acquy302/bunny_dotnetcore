﻿using Microsoft.Extensions.DependencyInjection;

namespace Bunny.DependencyInjection.Attributes
{
    public class PerResolveDependencyAttribute : DependencyAttribute
    {
        public PerResolveDependencyAttribute() : base(ServiceLifetime.Transient)
        {
        }
    }
}