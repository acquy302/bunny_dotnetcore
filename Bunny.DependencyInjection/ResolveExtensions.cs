﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Bunny.DependencyInjection
{
    public static class ResolveExtensions
    {
        /// <summary>
        ///     Resolve the registered implementation for the service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IServiceCollection services) where T : class
        {
            if (Resolver.Resolve<T>() == null)
            {
                Resolver.Services = services;
            }

            return Resolver.Resolve<T>();
        }

        /// <summary>
        ///     Resolve the registered implementation for the service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="applicationBuilder"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IApplicationBuilder applicationBuilder) where T : class
        {
            return applicationBuilder.ApplicationServices.Resolve<T>();
        }

        /// <summary>
        ///     Resolve the registered implementation for the service provider 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IServiceProvider serviceProvider) where T : class
        {
            return serviceProvider.GetService<T>();
        }
    }
}